
<?php include 'actionvaccines.php'; ?>
<!DOCTYPE html>

<html>
<head>
	<title>Your Website</title>
</head>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="">
<body>
	<div class="container">
		<div class="jumbotron">
			<h1>Medicine Stock <small>RK Tutorial</small></h1>
		</div>
	</div>
	<td><a href="Vaccines.php" class="btn btn-info">Go to Vaccines</a></td>
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="panel-heading">Enter Vaccine Details</div>
				<div class="panel-body">
					<form method="post" action="actionvaccines.php">
						<table class="table table-hover">
							<tr>
								<td>Vaccine Name</td>
								<td><input type="text" class="form-control" name="name" placeholder="Enter Medice Name"></td>
							</tr>
							<tr>
								<td>Quantity</td>
								<td><input type="text" class="form-control" name="qty" placeholder="Enter Quantity"></td>
							</tr>
							<tr>
							
								<td colspan="2" align="center"><input type="submit" class="btn btn-primary" name="submit" value="Store"></td>
							</tr>
						</table>
					</form>
				</div>
		</div>
	</div>
		<div class="col-md-3"></div>
	</div>
</div>

	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
				<table class="table table-bordered table-hover">
					<tr>
						<th>#</th>
						<th>Vaccine Name</th>
						<th>Available Stock</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
					</tr>
					<?php 
						$myrow = $dataOperation2->fetch_record("vaccines");
						foreach ($myrow as $row) {
						//breaking point
							?>
							<tr>
						<td><?php echo $row["vacID"]; ?></td>
						<td><?php echo $row["vacName"]; ?></td>
						<td><b><?php echo $row["vacQty"]; ?></b></td>
						<td><a href="#" class="btn btn-info">Edit</td>
						<td><a href="#" class="btn btn-danger">Delete</td>
					</tr>
						
					<?php
				}
			?>
					
				</table>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</body>
</html>