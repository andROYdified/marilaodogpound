<?php

include '../template/headeruser.php';
include "action.php";

?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<title>Dog Registration</title>
<link rel="stylesheet" href="" type="text/css" />
<script type="text/javascript"></script>
</head>

<body style="background-color: #FFECB3">

<!--<div class="container">
<div class="jumbotron">
<h1>Medicine Stock <small>RK Tuturial</small></h1>
</div>
</div>-->
<div class="container">
<div class="row">
<div class="col-md-3"></div>
<div class="col-md-6">
<div class="panel panel-primary">
<div class="panel-heading">Dog Registration</div>
<div class="panel-body">
<?php
if(isset($_GET["update"])){
//php 7
$id = $_GET["id"] ?? null;
$where = array("id"=>$id,);
$row = $obj->select_record("medicines",$where);
?>
<form method="post" action="action.php">
<table class="table table-hover">
<tr>
<td><input type="hidden" name="id" value="<?php echo $id; ?>"></td>
</tr>
<tr>
<td>Dog name</td>
<td><input type="text" class="form-control" value="" name="name" placeholder="Enter Medicine name"></td>
</tr>
<tr>

<td>Dog type</td>
<td><input type="text" class="form-control" name="qty" value="" placeholder="Enter Quantity"></td>
</tr>

<td>Dog Gender</td>
<td><input type="text" class="form-control" value="" name="name" placeholder="Enter Medicine name"></td>
</tr>
<tr>

<td>Breed</td>
<td><input type="text" class="form-control" name="qty" value="" placeholder="Enter Quantity"></td>
</tr>

<td>Color</td>
<td><input type="text" class="form-control" value="" name="name" placeholder="Enter Medicine name"></td>
</tr>
<tr>

<td>Markings</td>
<td><input type="text" class="form-control" name="qty" value="" placeholder="Enter Quantity"></td>
</tr>

<td>Medicine Name</td>
<td><input type="text" class="form-control" value="" name="name" placeholder="Enter Medicine name"></td>
</tr>
<tr>

<td>Size</td>
<td><input type="text" class="form-control" name="qty" value="" placeholder="Enter Quantity"></td>
</tr>

<td>Age</td>
<td><input type="text" class="form-control" value="" name="name" placeholder="Enter Medicine name"></td>
</tr>
<tr>

<td>Image</td>
<td><input type="file" class="form-control" value="" name="name" placeholder="Enter Medicine name"></td>
</tr>
<tr>
<td colspan="2" align="center"><input type="submit" class="btn btn-primary" name="edit" value="Update"></td>
</tr>
<tr>

</table>
</form>

<?php
}else{
?>
<form method="post" action="action.php">
<table class="table table-hover">
<tr>
<td><input type="hidden" name="id" value="<?php echo $id; ?>"></td>
</tr>
<tr>
<td>Dog name</td>
<td><input type="text" class="form-control" value="<?php echo $row["m_name"]; ?>" name="name" placeholder="Enter Medicine name"></td>
</tr>
<tr>

<td>Dog type</td>
<td><input type="text" class="form-control" name="qty" value="<?php echo $row["qty"]; ?>" placeholder="Enter Quantity"></td>
</tr>

<td>Dog Gender</td>
<td><input type="text" class="form-control" value="<?php echo $row["m_name"]; ?>" name="name" placeholder="Enter Medicine name"></td>
</tr>
<tr>

<td>Breed</td>
<td><input type="text" class="form-control" name="qty" value="<?php echo $row["qty"]; ?>" placeholder="Enter Quantity"></td>
</tr>

<td>Color</td>
<td><input type="text" class="form-control" value="<?php echo $row["m_name"]; ?>" name="name" placeholder="Enter Medicine name"></td>
</tr>
<tr>

<td>Markings</td>
<td><input type="text" class="form-control" name="qty" value="<?php echo $row["qty"]; ?>" placeholder="Enter Quantity"></td>
</tr>

<td>Medicine Name</td>
<td><input type="text" class="form-control" value="<?php echo $row["m_name"]; ?>" name="name" placeholder="Enter Medicine name"></td>
</tr>
<tr>

<td>Size</td>
<td><input type="text" class="form-control" name="qty" value="<?php echo $row["qty"]; ?>" placeholder="Enter Quantity"></td>
</tr>

<td>Age</td>
<td><input type="text" class="form-control" value="<?php echo $row["m_name"]; ?>" name="name" placeholder="Enter Medicine name"></td>
</tr>
<tr>

<td>Image</td>
<td><input type="file" class="form-control" value="<?php echo $row["m_name"]; ?>" name="name" placeholder="Enter Medicine name"></td>
</tr>
<tr>
<td colspan="2" align="center"><input type="submit" class="btn btn-primary" name="edit" value="Update"></td>
</tr>
<tr>
</table>
</form>


<?php
}

?>


</div>
</div>
</div>
<div class="col-md-3"></div>
</div>

</div>

<div class="container">
<div class="row">
<div class="col-md-2"></div>
<div class="col-md-8">
<table class="table table-bordered">
<tr>
<th>#</th>
<th>Medicine Name</th>
<th>Available Stock</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
</tr>
<?php
$myrow = $obj->fetch_record("medicines");
foreach ($myrow as $row) {
//breaking point
?>
<tr>
<td><?php echo $row["id"]; ?></td>
<td><?php echo $row["m_name"]; ?></td>
<td><b><?php echo $row["qty"]; ?></b></td>
<td><a href="dogregistration.php?update=1&id=<?php echo $row["id"]; ?>" class="btn btn-info">Edit</a></td>
<td><a href="action.php?delete=1&id=<?php echo $row["id"]; ?>" onclick="return confirm('Do you want to delete this record?');" class="btn btn-danger">Delete</a></td>
</tr>


<?php
}

?>

</table>
</div>
<div class="col-md-2"></div>
</div>
</div>

</body>
</html>