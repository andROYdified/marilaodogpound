<?php include '../template/headeruser.php'




?>

<!DOCTYPE html>
<html>
<head>

<title>Dog Registration</title>

</head>

<body style="background-color: #FFECB3">
	
<div class="form">
	<h2>Dog Registration</h2>
	<hr>
	<p id="msg"></p>
	<!--Form start here-->
	<form action="submit.php" method="post" id="register_form" enctype="multipart/form-data" >
	<label>dog ID</label>
	<div class="input">
		<div>
			<input type="text" name="dogID" id="name" readonly>
		</div>
		<div class="error u_error"></div>
	</div>
	<label>user ID</label>
	<div class="input">
		<div>
			<input type="text" name="" id="name" value="<?php echo $_SESSION['userID']; ?>" >
		</div>
		<div class="error u_error"></div>
	</div>

	<label>Upload Dog Picture</label>
		<div class="input">
		<div>
			<input type="file"  class="btn btn-success" name="dogImage">
		</div>
		<div class="error u_error"></div>
	</div>
<br>
	<label>Dog name</label>
	<div class="input">
		<div>
			<input type="text" name="dogName" value="">
		</div>
	</div>


	<label>Dog Type</label>
	<div class="input">
		<div>
			<select class="input-opt" name="dogType">
				<option value="PUPPY">PUPPY</option>
				<option value="ADULTDOG">ADULT DOG</option>
				
			</select>
		</div>

		<div class="error"></div>
	</div>

	<label>Gender</label>
	<div class="input">
		<div>
			<select class="input-opt" name="dogGender">
				<option value="MALE">MALE</option>
				<option value="FEMALE">FEMALE</option>
			</select>
		</div>

		<div class="error"></div>
	</div>

		<label>Breed</label>
	<div class="input">
		<div>
			<select class="input-opt" name="dogBreed">
				<option value="PUREBREED">PURE BREED</option>
				<option value="MONGREL">MONGREL</option>
				<option value="MIXEDBREED">MIXED BREED</option>
			</select>
		</div>

		<div class="error"></div>
	</div>

	

		<label>Color</label>
	<div class="input">
		<div>
			<select class="input-opt" name="dogColor">
				<option value="WHITE">WHITE</option>
				<option value="BLACK">BLACK</option>
				<option value="BROWN">BROWN</option>
				<option value="MIXED">MIXED</option>
			</select>
		</div>

		<div class="error"></div>
	</div>
	<label>Markings</label>
	<div class="input">
		<div>
			<input type="text" name="dogMarkings" value="">
		</div>
	</div>


	<label>Size</label>
	<div class="input">
		<div>
			<select class="input-opt" name="dogSize">
				<option value="SMALL">SMALL</option>
				<option value="MEDIUM">MEDIUM</option>
				<option value="LARGE">LARGE</option>
				
			</select>
		</div>

		<div class="error"></div>
	</div>

	<label>Age</label>
	<div class="input">
		<div>
			<input type="text" name="dogAge" value="">
		</div>
	</div>

		<label>Status</label>
	<div class="input">
		<div>
			<select class="input-opt" name="dogStatus">
				<option value="NOT VERIFIED">NOT VERIFIED</option>
				<option value="VERIFIED">VERIFIED</option>
				
			</select>
		</div>

		<div class="error"></div>
	</div>



	
	<div class="input">
		<div>
			<input type="submit" class="btn btn-primary" name="submit" value="Submit">
		</div>
	</div>
	</form>
</div>

</body>
</html>
<?php include '../template/footer.php'; ?>