<?php include('header.php');
require_once('db.php');
 ?>
<body>


    <div class="row-fluid">
        <div class="span12">


         

            <div class="container">
			
<?php //include ('modal_add.php'); ?>

                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                            <div class="alert alert-info">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong><i class="icon-user icon-large"></i>&nbsp;Dog Information </strong>
                            </div>
                            <thead>
                               <!-- <tr>
                                    <th style="text-align:center;">DogID</th>-->
                                    <th style="text-align:center;">#</th>
                                    <th style="text-align:center;">Image</th>
                                     <th style="text-align:center;">Dog's Name</th>                                
                                     <th style="text-align:center;">Status</th>                                
                                     <th style="text-align:center;">Lastname</th>                                
                                     <th style="text-align:center;">Firstname</th>                                
                                     <th style="text-align:center;">Middlename</th>                                
                                     <th style="text-align:center;">Contact No.</th>                                
                                     <th style="text-align:center;">Reason</th>                                
                                     <th style="text-align:center;">Date Register</th>                                
                                     <th style="text-align:center;">Action</th>                                
                            </thead>
                            <tbody>
							<?php
								
								$result = $conn->prepare("SELECT * FROM dogfiles ORDER BY id ASC");
								$result->execute();
								for($i=0; $row = $result->fetch(); $i++)
								{
								$id=$row['id'];
								$results = $conn->prepare("SELECT status FROM dogstatus where id=".$row['status']);
								$results->execute();
									for($ii=0; $srow = $results->fetch(); $ii++){
														$dstatus=$srow['status'];															
									}									

							?>
								<tr>																
								
								<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['id']; ?></td>
								<td style="text-align:center; margin-top:10px; word-break:break-all; width:450px; line-height:100px;">
								<?php 
								$picture="../uploads/default.png";
								if($row['picture'] != "" ) $picture='../uploads/'.$row['picture'];										
								?>
								<img src="<?php echo $picture; ?>" width="100px" height="100px" style="border:1px solid #333333;">
								</td>
								

								<a href="#updte_img<?php echo $id;?>"  data-toggle="modal"  class="btn btn-warning" >Update</a>
									 <br>									 
									 <br>									 
									 <a href="#delete<?php echo $id;?>"  data-toggle="modal"  class="btn btn-danger" >Delete </a>
								</td>
								</tr>
										<!-- Modal -->
							<div id="delete<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-header">
							<h3 id="myModalLabel">Delete</h3>
							</div>
							<div class="modal-body">
							<div class="alert alert-danger">
							<?php 
								$picture="..uploads/default.png";
								if($row['picture'] != "" ) $picture='../uploads/'.$row['picture'];										
								?>
								<img src="<?php echo $picture; ?>" width="100px" height="100px" style="border:1px solid #333333;">	
							<b style="color:blue; margin-left:25px; font-size:30px;"><?php echo $row['id']." ".$row['dogname']; ?></b>
							<br />
							<p style="font-size: larger; text-align: center;">Are you Sure you want to Delete?</p>
							</div>
							<hr>
							<div class="modal-footer">
							<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
							<a href="delete.php<?php echo '?id='.$id; ?>" class="btn btn-danger">Yes</a>
							</div>
							</div>
							</div>
						<!-- Modal Update Image -->
							<div id="updte_img<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-header"><h3 id="myModalLabel">Update</h3></div>

							<div class="modal-body">
							<div class="alert alert-danger">
							
							<form action="edit.php<?php echo '?id='.$id; ?>" method="post" enctype="multipart/form-data">											
								
								<!--<label style="color:#3a87ad; font-size:18px;" >Upload Picture</label>-->
									
								<?php 
								$picture="..uploads/default.png";
								if($row['picture'] != "" ) $picture='../uploads/'.$row['picture'];										
								?>
								<img src="<?php echo $picture; ?>" width="100px" height="100px" style="border:1px solid #333333;">				
								<div style="color:blue; margin-left:150px; font-size:30px;">
								<input type="file" name="picture" style="margin-top:-115px;" value="<?php echo $row['picture'];?>"></div>

								<label style="color:#3a87ad; font-size:18px;" >Dogs Name</label>								
									<input type="text" name="dogname" value="<?php echo $row['dogname'];?>">							
								<label style="color:#3a87ad; font-size:18px;" >Status</label>								
								<select  name="status" style="">
									<?php 
										$resulte = $conn->prepare("SELECT * FROM dogstatus ORDER BY id ASC");
										$resulte->execute();
										for($ie=0; $erow = $resulte->fetch(); $ie++){
											echo '<option value="'.$id=$erow['id'].'">'.$id=$erow['status'].'</option>';
									}?>
								</select>	
								
								<label style="color:#3a87ad; font-size:18px;" >Lastname</label>
									<input type="text" name="lastname" value="<?php echo $row['lastname'];?>">							
								<label style="color:#3a87ad; font-size:18px;" >Firstname</label>
									<input type="text" name="firstname" value="<?php echo $row['firstname'];?>">							
								<label style="color:#3a87ad; font-size:18px;" >Middlename</label>
									<input type="text" name="middlename" value="<?php echo $row['middlename'];?>">							
								<label style="color:#3a87ad; font-size:18px;" >Contact No.</label>
									<input type="text" name="contactno" value="<?php echo $row['contactno'];?>">															
								<label style="color:#3a87ad; font-size:18px;" >Reason</label>
									<input type="text" name="reason" value="<?php echo $row['reason'];?>">															
																														
							</div>

							<hr>
							<div class="modal-footer">
							<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
							<button type="submit" name="submit" class="btn btn-danger">Yes</button>
							</form>
							</div>
							</div>
							</div>
								<?php } ?>
                            </tbody>
                        </table>


          
        </div>
        </div>
        </div>
    </div>


</body>
</html>


