<?php 
include('header.php');
require_once('../database/db.php');
$theader=array(
				'#',
				'Veterinarian',
				'client Image',
				'client Name',
				'dog Name',
				'Breed',
				'Description',
				'Vaccine',
				'Date/Time'				
			);
			$tname=array(	
				'brgystatID',
				'veterinarian',
				'clientImage',
				'clientName',
				'dognName',
				'breed',
				'description',
				'vaccine',
				'vtime'
				
			);
			$cnt=count($theader);
?>
<body>

<div class="row-fluid">
	<div class="span12">
		<div class="container">
		<a class="btn btn-primary" href="#myModal" data-toggle="modal">Click Here To Add</a>
		<br><br>
		<a href="../administrator/navigationinline.php"><button class="btn btn-primary">Back</button></a>
<!-- Modal -->
	<div id="myModal" class="modal hide fade " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	<div class="modal-header"><h3 id="myModalLabel">Add</h3></div>
		<div class="modal-body">

		<form method="post" action="add.php"  enctype="multipart/form-data">
		<table class="table1">

		<?php 
			

			
			for ($row=1;$row<$cnt-1;$row++) 
				{
					if ($row==1) 
						{
							echo '<tr><td><label style="color:#3a87ad; font-size:18px;">Veterinarian</label></td><td width="30"></td>
							<td><select  name="veterinarian" style="">';						
							$resulta = $con->prepare("SELECT acctID,acctName FROM account ORDER BY acctName ASC");
							$resulta->execute();
							for($ia=0; $rowa = $resulta->fetch(); $ia++){echo '<option value="'.$rowa['acctID'].'">'.$rowa['acctName'].'</option>';}	
							echo ' </select></td></tr>';						
						}
					
					else {echo '
							<tr><td><label style="color:#3a87ad; font-size:18px;" >'.$theader[$row].'</label></td>
							<td width="30"></td>
							<td><input type="text" name="'.$tname[$row].'" value="" placeholder="Enter '.$theader[$row].'"></td>';
						}
				}

			echo '</tr>';
			
		?>
		</table>
		</div>


		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button type="submit" name="Submit" class="btn btn-primary">Upload</button>
		</div>
		</form>
</div>			


<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
	<div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<strong><i class="icon-user icon-large"></i>&nbsp;Statistical Information</strong>
	</div>
	<thead>

	<?php
	for ($row=0;$row<$cnt;$row++) {echo '<th style="text-align:center;">'.$theader[$row].'</th>';}
	?>

	<th style="text-align:center;">Action</th>                                
	</thead>

	<tbody>
	<tr>

	<?php
	$result = $con->prepare("SELECT * FROM brgystatisticalreport ORDER BY brgystatID ASC");
	$result->execute();
	for($i=0; $row = $result->fetch(); $i++){
		$id=$row['brgystatID'];
		$slbl='<td style="text-align:center; word-break:break-all; width:200px;">';
		for ($xrow=0;$xrow<$cnt;$xrow++) 
		{
			if ($xrow==1) 
			{
				$resultsx = $con->prepare("SELECT * FROM account where acctID=".$row['veterinarian']);
				$resultsx->execute();
				for($iix=0; $rowsx = $resultsx->fetch(); $iix++){ echo $slbl.$rowsx['acctName'].'</td>'; }				
			}
		else 
			{echo $slbl.$row[$tname[$xrow]].'</td>';}
		}	
	?>

		<td style="text-align:center; width:350px;">
		<a href="#updte_img<?php echo $id;?>"  data-toggle="modal"  class="btn btn-warning" >Update</a>									 
		<a href="#delete<?php echo $id;?>"  data-toggle="modal"  class="btn btn-danger" >Delete </a>
		</td>
	</tr>
<!-- Modal -->
<div id="delete<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header"><h3 id="myModalLabel">Delete</h3></div>
	<div class="modal-body">
	<div class="alert alert-danger">
  
	<b style="color:blue; margin-left:25px; font-size:30px;"><?php echo $row['clientName']." ".$row['dognName']; ?></b>
	<br />
	<p style="font-size: larger; text-align: center;">Are you Sure you want to Delete?</p>
</div>
	<hr>
	<div class="modal-footer">
		<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
		<a href="delete.php<?php echo '?id='.$id; ?>" class="btn btn-danger">Yes</a>
	</div>
</div>
</div>
<!-- Modal Update Image -->
<div id="updte_img<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header">
<h3 id="myModalLabel">Update</h3>

</div>

<div class="modal-body">
	<div class="alert alert-danger">

		<form action="edit.php<?php echo '?id='.$id; ?>" method="post" enctype="multipart/form-data">

			<?php

			for ($xrow=1;$xrow<$cnt-1;$xrow++) 
				{
					if ($xrow==1) 
						{											
						echo '<select  name="veterinarian" style="">';						
						$resulta = $con->prepare("SELECT * FROM account ORDER BY acctID ASC");
						$resulta->execute();
						for($ia=0; $rowa = $resulta->fetch(); $ia++)
							{if ($rowa['acctID']==$row[$tname[$xrow]])
								{echo '<option value="'.$rowa['acctID'].'" selected>'.$rowa['acctName'].'</option>';}
							else
								{echo '<option value="'.$rowa['acctID'].'">'.$rowa['acctName'].'</option>';}																
							}	
							echo '		</select>
							</td>		
							</tr>';
						}
						else 
						{
							echo '<label style="color:#3a87ad; font-size:18px;" >'.$theader[$xrow].'</label>';							
							echo '<input type="text" name="'.$tname[$xrow].'" value="'.$row[$tname[$xrow]].'">';
						}
				}
			?>
			</div>
			<hr>
			<div class="modal-footer">
			<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
			<button type="submit" name="submit" class="btn btn-danger">Yes</button>
		</form>
	</div>
</div>
</div>
<?php } ?>
</tbody>
</table>



</div>
</div>
</div>
</div>


</body>
</html>


