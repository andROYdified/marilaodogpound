<?php 
include('header.php');
include ('../database/db.php');
session_start();
$uid=$_SESSION['userID'];

$theader=array(
'#',
'Veterinarian',
'client Image',
'client Name',
'dog Name',
'Breed',
'Description',
'Vaccine',
'Date/Time'				
);
$tname=array(	
'brgystatID',
'veterinarian',
'dogID',
'description',
'vaccine',
'vtime'

);

function getrec($sql) 
{
	include ('../database/db.php');
	$sth = $conn->prepare($sql);
	$sth->execute();
	$result = $sth->fetchall(PDO::FETCH_BOTH);
	$xdata=$result;
	return $xdata;	
}

$cnt=count($theader);
?>
<body>

<div class="row-fluid">
	<div class="span12">
		<div class="container">
		<a class="btn btn-primary" href="#myModal" data-toggle="modal">Click Here To Add</a><br><br><a href="../administrator/navigationinline.php"><button class="btn btn-primary">Back</button></a>
<!-- Modal -->
	<div id="myModal" class="modal hide fade " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	<div class="modal-header"><h3 id="myModalLabel">Add</h3></div>
		<div class="modal-body">

		<form method="post" action="add.php"  enctype="multipart/form-data">
		<table class="table1">

		<?php 
					echo '<input type="hidden"  value="'.$uid.'" name="veterinarian">';
										
							$sql="SELECT Dogregistration.*
									FROM Dogregistration LEFT JOIN Brgystatisticalreport ON Dogregistration.dogID = Brgystatisticalreport.dogID
									WHERE Brgystatisticalreport.dogID Is Null";
							$Dogregistration=getrec($sql);
							
							echo '<tr><td><label style="color:#3a87ad; font-size:18px;">Dog Name</label></td>
							<td width="30"></td>
							<td><select  name="dogID" >';
							
							foreach($Dogregistration as $dreg)
								{echo '<option value="'.$dreg['dogID'].'">'.$dreg['dogName'].'</option>';}									
							echo ' </select></td></tr>';																		
							
						echo '<tr><td><label style="color:#3a87ad; font-size:18px;">Description</label></td>
							<td width="30"></td>
							<td><input type="text"  name="description"></td>';
							
						echo '<tr><td><label style="color:#3a87ad; font-size:18px;">vaccine</label></td>
							<td width="30"></td>
							<td><input type="text"  name="vaccine"></td>';
							
						echo '<tr><td><label style="color:#3a87ad; font-size:18px;">Date/Time</label></td>
							<td width="30"></td>
							<td><input type="date"  name="vtime"></td>';
				
			
			echo '</tr>';
			
		?>
		</table>
		</div>


		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button type="submit" name="Submit" class="btn btn-primary">Upload</button>
		</div>
		</form>
</div>			


<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
	<div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<strong><i class="icon-user icon-large"></i>&nbsp;Statistical Information</strong>
	</div>
	<thead>

	<?php
	for ($row=0;$row<$cnt;$row++) {echo '<th style="text-align:center;">'.$theader[$row].'</th>';}
	?>

	<th style="text-align:center;">Action</th>                                
	</thead>

	<tbody>
	<tr>

	<?php
	$sql="SELECT * FROM brgystatisticalreport where veterinarian=".$uid." ORDER BY brgystatID ASC";
	$brgystatisticalreport=getrec($sql);	
	foreach($brgystatisticalreport as $brinfo)
	{
		//echo $brinfo['brgystatID'];
		$id=$brinfo['brgystatID'];
		$slbl='<td style="text-align:center; word-break:break-all; width:200px;">';
		for ($xrow=0;$xrow<6;$xrow++) 
		{
			$cpicture='';
			$cname='';
			$dname='';
			$dbreed='';
			if ($xrow==2) 
			{
				$sql="SELECT * FROM dogregistration where dogID=".$brinfo['dogID'];
				$resultsx = $conn->prepare($sql);
				$resultsx->execute();
				for($iix=0; $rowsx = $resultsx->fetch(); $iix++)
					{
						
						$dname=$rowsx['dogName'];
						$dbreed=$rowsx['dogBreed'];
						$duserID=$rowsx['userID'];
						
					}
						$sql="SELECT * FROM userinfo where userID=".$duserID;
						$resultsx = $conn->prepare($sql);
						$resultsx->execute();
						for($iix=0; $rowsx = $resultsx->fetch(); $iix++)
							{
								$cpicture=$rowsx['userImage'];
								$cname=$rowsx['userLastname'].', ';
								$cname.=$rowsx['userFirstname'].' ';
								$cname.=$rowsx['userMiddlename'];																
								
							}
					echo $slbl.$cpicture.'</td>';
					echo $slbl.$cname.'</td>';								
					echo $slbl.$dname.'</td>';
					echo $slbl.$dbreed.'</td>';
					
			}
		elseif ($xrow==1) 
			{
				$sql="SELECT * FROM userinfo where userid=".$brinfo['veterinarian'];
				$userinfo=getrec($sql);	
				foreach($userinfo as $uinfo)

					{
						//$cpicture=$rowsx['userImage'];
						$cname=$uinfo['userLastname'].', ';
						$cname.=$uinfo['userFirstname'].' ';
						$cname.=$uinfo['userMiddlename'];																
						//echo $slbl.$cpicture.'</td>';
						echo $slbl.$cname.'</td>';
					}	
					
			}
		
		else
			{echo $slbl.$brinfo[$tname[$xrow]].'</td>';echo $xrow;}
		}	
	?>

		<td style="text-align:center; width:350px;">
		<a href="#updte_img<?php echo $id;?>"  data-toggle="modal"  class="btn btn-warning" >Update</a>									 
		<a href="#delete<?php echo $id;?>"  data-toggle="modal"  class="btn btn-danger" >Delete </a>
		</td>
	</tr>
<!-- Modal -->
<div id="delete<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header"><h3 id="myModalLabel">Delete</h3></div>
	<div class="modal-body">
	<div class="alert alert-danger">
  
	<b style="color:blue; margin-left:25px; font-size:30px;"><?php echo $row['clientName']." ".$row['dognName']; ?></b>
	<br />
	<p style="font-size: larger; text-align: center;">Are you Sure you want to Delete?</p>
</div>
	<hr>
	<div class="modal-footer">
		<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
		<a href="delete.php<?php echo '?id='.$id; ?>" class="btn btn-danger">Yes</a>
	</div>
</div>
</div>
<!-- Modal Update Image -->
<div id="updte_img<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header">
<h3 id="myModalLabel">Update</h3>

</div>

<div class="modal-body">
	<div class="alert alert-danger">

		<form action="edit.php<?php echo '?id='.$id; ?>" method="post" enctype="multipart/form-data">

			<?php

			echo '<input type="hidden"  value="'.$uid.'" name="veterinarian">';
							
							$sql="SELECT * FROM brgystatisticalreport where brgystatID=".$id;
							$brgystatisticalreport=getrec($sql);
							foreach($brgystatisticalreport as $dbar)
								{
									$description=$dbar['description'];
									$vaccine=$dbar['vaccine'];
									$vtime=$dbar['vtime'];
								}
								$sql="SELECT Dogregistration.*
										FROM Dogregistration LEFT JOIN Brgystatisticalreport ON Dogregistration.dogID = Brgystatisticalreport.dogID
										WHERE Brgystatisticalreport.dogID Is Null or Brgystatisticalreport.dogID=".$dbar['dogID'];
								$Dogregistration=getrec($sql);
								
								echo '<label style="color:#3a87ad; font-size:18px;">Dog Name</label>
								
								<select  name="dogID" value="'.$dbar['dogID'].'">';
								
								foreach($Dogregistration as $dreg)
									{echo '<option value="'.$dreg['dogID'].'">'.$dreg['dogName'].'</option>';}									
								echo ' </select>';																		
								
								echo '<label style="color:#3a87ad; font-size:18px;">Description</label>
									
									<input type="text"  name="description" value="'.$description.'">';
									
								echo '<label style="color:#3a87ad; font-size:18px;">vaccine</label>
									
									<input type="text"  name="vaccine" value="'.$vaccine.'">';
									
								echo '<label style="color:#3a87ad; font-size:18px;">Date Time</label>
									
									<input type="date"  name="vtime" value="'.$vtime.'" >';
								
				
			
			echo '</tr>';
			?>
			</div>
			<hr>
			<div class="modal-footer">
			<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
			<button type="submit" name="submit" class="btn btn-danger">Yes</button>
		</form>
	</div>
</div>
</div>
<?php } ?>
</tbody>
</table>



</div>
</div>
</div>
</div>


</body>
</html>


