<?php 
include('header.php');
require_once('db.php');
session_start();
$uid=$_SESSION['userID'];
$theader=array(
	'User ID',
	'Account ID',
	'UserImage',
	'Firstname',
	'Middlename',		
	'Lastname',
	'Block Number',
	'Lot Number',	
	'Street',
	'Barangay',
	'Municipality',
	'Province',
	'Contact Number',
	'Bdate',
	'Gender',
	'Status'		
	);
$tname=array(	
	'userID',
	'acctID',
	'userImage',
	'userFirstname',
	'userMiddlename',
	'userLastname',	
	'userBlkno',
	'userLotno',
	'userStreet',
	'userBarangay',
	'userMunicipality',	
	'userProvince',	
	'userContactno',
	'userBdate',
	'userGender',
	'userStatus'
	);
$cnt=count($theader);

$sql = "SELECT userID,
acctID,
userImage,
userFirstname,
userMiddlename,
userLastname,
userBlkno,
userStreet,
userBarangay,
userMunicipality,
userProvince,
userContactno,
userBdate,
userGender,
userStatus

FROM userinfo where userID=".$uid." ORDER BY userLastname ASC";
$result = $conn->prepare($sql);
$result->execute();				
$row = $result->fetchall();			
//$accntname= $row[0][1];

$result = $conn->prepare("SELECT * FROM userinfo ORDER BY userID ASC");
$result->execute();
$xrow = $result->fetchall();	


?>
<body>


<div class="row-fluid">
	<div class="span12">
		<div class="container">

		<!-- Button to trigger modal -->
		<a class="btn btn-primary" href="#myModal" data-toggle="modal">Click Here To Add</a>
		<br>
		<br>
		<a href="../administrator/navigationinline.php"><button class="btn btn-primary">Back</button></a>
		<!-- Modal ADD-->
		<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header"><h3 id="myModalLabel">Add Events</h3></div>
			<div class="modal-body">

			<form method="post" action="add.php"  enctype="multipart/form-data">
				<table class="table1">

						<input type="hidden" name="<?=$tname[4]?>" value="<?=$uid?>" >				
						<tr><td><label style="color:#3a87ad; font-size:18px;" ><?=$theader[1]?></label></td><td width="30"></td>				
						<td><input type="file" name="<?=$tname[1]?>" value="" ></td></tr>										
						
						<tr><td><label style="color:#3a87ad; font-size:18px;" ><?=$theader[2]?></label></td><td width="30"></td>				
						<td><input type="text" name="<?=$tname[2]?>" value="" placeholder="Enter <?=$theader[2]?>"></td></tr>
						
						<tr><td><label style="color:#3a87ad; font-size:18px;" ><?=$theader[3]?></label></td><td width="30"></td>
						<td><textarea name="<?=$tname[3]?>" value="" placeholder="Enter <?=$theader[3]?>" row="5"></textarea></td></tr>
														
						<tr><td><label style="color:#3a87ad; font-size:18px;" ><?=$theader[5]?></label></td><td width="30"></td>				
						<td><input type="date" name="<?=$tname[5]?>" value="" placeholder="Enter <?=$theader[5]?>"></td></tr>
						
						<tr><td><label style="color:#3a87ad; font-size:18px;" ><?=$theader[4]?></label></td><td width="30"></td>										
						<td><input type="text" name="unm" value="<?=$accntname?>" disabled></td></tr>	
				</table>
			</div>

			<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button type="submit" name="Submit" class="btn btn-primary">Upload</button>
			</div>
			</form>
		</div>		
			
		<!-- LISTING -->
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong><i class="icon-user icon-large"></i>&nbsp;Events</strong>
		</div>
		<thead>
		<?php for ($row=0;$row<$cnt;$row++) {echo '<th style="text-align:center;">'.$theader[$row].'</th>';}?>
		<th style="text-align:center;">Action</th>                                
		</thead>
		 <tbody>
				<?php
					foreach($xrow as $row)
					{$id=$row[0];
				?>
					<tr>																
					
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row [0]; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row [1]; ?></td>
					<td style="text-align:center; margin-top:10px; word-break:break-all; width:450px; line-height:100px;">
					<?php $picture="../uploads/default.png";if($row[2] != "" ) $picture='../uploads/'.$row[2];?>
					<img src="<?php echo $picture; ?>" width="100px" height="100px" style="border:1px solid #333333;"></td>								
					
				<!--	<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row [2]; ?></td>	-->									
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row [3]; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row [4]; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row [5]; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row [6]; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row [7]; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row [8]; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row [9]; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row [10]; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row [11]; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row [12]; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row [13]; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row [14]; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row [15]; ?></td>
				
					<td style="text-align:center; width:350px;">
						 <a href="#updte_img<?php echo $id;?>"  data-toggle="modal"  class="btn btn-warning" >Update</a>
						 <br>									 
						 <br>									 
						 <a href="#delete<?php echo $id;?>"  data-toggle="modal"  class="btn btn-danger" >Delete </a>
					</td>
					</tr>
							<!-- Modal -->
				<div id="delete<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-header">
				<h3 id="myModalLabel">Delete</h3>
				</div>
				<div class="modal-body">
				<div class="alert alert-danger">
				<?php 
					$picture="../uploads/default.png";
					if($row['userImage'] != "" ) $picture='../uploads/'.$row['UserImage'];										
					?>
					<img src="<?php echo $picture; ?>" width="100px" height="100px" style="border:1px solid #333333;">	
				<b style="color:blue; margin-left:25px; font-size:30px;"><?php echo 'UserID : '.$row['userID']." User Lastname :".$row['userLastname']; ?></b>
				<br />
				<p style="font-size: larger; text-align: center;">Are you Sure you want to Delete?</p>
				</div>
				<hr>
				<div class="modal-footer">
				<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
				<a href="delete.php<?php echo '?id='.$id; ?>" class="btn btn-danger">Yes</a>
				</div>
				</div>
				</div>
									

		<!-- Modal Update Image -->
			<div id="updte_img<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header"><h3 id="myModalLabel">Update</h3></div>

				<div class="modal-body">
					<form action="edit.php<?php echo '?id='.$id; ?>" method="post" enctype="multipart/form-data">												
					<?php $picture="..uploads/default.png";if($row['userImage'] != "" ) $picture='../uploads/'.$row['userImage'];															?>											
					
					<input type="hidden" name="<?=$tname[4]?>" value="<?=$row[$tname[4]]?>">
					<label style="color:#3a87ad; font-size:18px;" ><?=$theader[1]?></label>	
					<img src="<?=$picture?>" width="100px" height="100px" style="border:1px solid #333333;">	
					
					<input type="file" name="picture" style="margin-top:-115px;" value="<?=$row[$tname[1]]?>">
					
					<label style="color:#3a87ad; font-size:18px;" ><?=$theader[2]?></label>	
					<input type="text" name="<?=$tname[2]?>" value="<?=$row[$tname[2]]?>">	
					
					<label style="color:#3a87ad; font-size:18px;" ><?=$theader[3]?></label>	
					<textarea type="textarea" name="<?=$tname[3]?>" value="<?=$row[$tname[3]]?>"> </textarea>	
					<label style="color:#3a87ad; font-size:18px;" ><?=$theader[5]?></label>	
					<input type="date" name="<?=$tname[5]?>" value="<?=$row[$tname[5]]?>">	
					
					
															
				</div>

				<hr>
				<div class="modal-footer">
					
				<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
				<button type="submit" name="submit" class="btn btn-danger">Yes</button>
				</form>
			</div>
		</div>
	</div>
	<?php } ?>
	</tbody>
	</table>

	</div>
</div>



</body>
</html>


