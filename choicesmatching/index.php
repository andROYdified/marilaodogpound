<?php include('header.php');
require_once('db.php');
?>
<body>

<div class="row-fluid">
<div class="span12">

<div class="container">


<!-- Button to trigger modal -->
<a class="btn btn-primary" href="#myModal" data-toggle="modal">Click Here To Add</a>
<br>
<br>
<br>
<a href="../administrator/navigationinline.php"><button class="btn btn-primary">Back</button></a>
<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header">

<h3 id="myModalLabel">Question Matching</h3>
</div>
<div class="modal-body">
<form method="post" action="add.php"  enctype="multipart/form-data">
<table class="table1">

<?php 
$shtm='<tr><td><label style="color:#3a87ad; font-size:18px;">';
$mhtm='</label></td>
<td width="30"></td>
<td><input type="text"';
$ehtm='></td>
</tr>';

//echo $shtm.'ID'.$mhtm.' name="id" placeholder="id" readonly="" '.$ehtm;
//echo $shtm.'Matching Question'.$mhtm.'matchingquestion name="matchingquestion" placeholder="matchingquestion"',$ehtm;
echo '<td><label style="color:#3a87ad; font-size:18px;">Matching Question	</label></td><td width="30"></td>
<td><select  name="matchingnumber" style="">';

require_once('db.php');
$resulta = $conn->prepare("SELECT * FROM questionmatching ORDER BY matchingNumber ASC");
$resulta->execute();
for($ia=0; $rowa = $resulta->fetch(); $ia++){
echo '<option value="'.$rowa['matchingNumber'].'">'.$rowa['matchingQuestion'].'</option>';
}	
echo '		</select>
</td>		
</tr>';

echo $shtm.'Matching Choices'.$mhtm.'matchingchoices name="matchingchoices" placeholder="matchingchoices"',$ehtm;
echo $shtm.'Matching Translation'.$mhtm.'matchingchoicestrans name="matchingchoicestrans" placeholder="matchingchoicestrans"',$ehtm;
echo $shtm.'Match Points'.$mhtm.'ismatch name="ismatch" placeholder="ismatch"',$ehtm;

?>	


</table>
</div>
<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
<button type="submit" name="Submit" class="btn btn-primary">Upload</button>
</div>
</form>
</div>			

<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
<div class="alert alert-info">
<button type="button" class="close" data-dismiss="alert">&times;</button>
<strong><i class="icon-user icon-large"></i>&nbsp;Dog Information </strong>
</div>
<thead>
<!-- <tr>
<th style="text-align:center;">DogID</th>-->
<th style="text-align:center;">#</th>
<th style="text-align:center;">Matching Question</th>
<th style="text-align:center;">Matching Choices</th>
<th style="text-align:center;">Matching Translation</th>
<th style="text-align:center;">Match Points</th>
<th style="text-align:center;">Action</th>                                 
</thead>
<tbody>
<?php

$result = $conn->prepare("SELECT * FROM choicesmatching order by matchingChoicesID asc");
$result->execute();
for($i=0; $row = $result->fetch(); $i++){
$id=$row['matchingChoicesID'];		
$idm=$row['matchingNumber'];		

$results = $conn->prepare("SELECT * FROM questionMatching where matchingNumber=".$idm);
$results->execute();
for($ii=0; $rows = $results->fetch(); $ii++){ $choicedesc=$rows ['matchingQuestion']; }
$shtm='<td style="text-align:center; word-break:break-all; width:200px;">';
echo '<tr>';
echo $shtm.$row ['matchingChoicesID'].'</td>';
echo $shtm.$choicedesc.'</td>';
echo $shtm.$row ['matchingChoices'].'</td>';
echo $shtm.$row ['matchingChoicesTrans'].'</td>';
echo $shtm.$row ['isMatch'].'</td>';			
?>	
							
<td style="text-align:center; width:350px;">
<a href="#updte_img<?php echo $id;?>"  data-toggle="modal"  class="btn btn-warning" >Update</a>																	 
<a href="#delete<?php echo $id;?>"  data-toggle="modal"  class="btn btn-danger" >Delete </a>
</td>
</tr>
<!-- Modal -->
<div id="delete<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header">
<h3 id="myModalLabel">Delete</h3>
</div>
<div class="modal-body">
<div class="alert alert-danger">
<b style="color:blue; margin-left:25px; font-size:30px;"><?php echo $row['matchingChoices']."/ ".$row['matchingChoicesTrans']; ?></b>
<br />
<p style="font-size: larger; text-align: center;">Are you Sure you want to Delete?</p>
</div>
<hr>
<div class="modal-footer">
<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
<a href="delete.php<?php echo '?id='.$id; ?>" class="btn btn-danger">Yes</a>
</div>
</div>
</div>
<!-- Modal Update Image -->
<div id="updte_img<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">			
<div class="modal-header"><h3 id="myModalLabel">Update</h3></div>
<div class="modal-body">
<div class="alert alert-danger">

<form action="edit.php<?php echo '?id='.$id; ?>" method="post" enctype="multipart/form-data">		
<label>Matching Question</label>
<?php 

echo '<select  name="matchingNumber" style="">';
require_once('db.php');
$resulta = $conn->prepare("SELECT * FROM questionmatching ORDER BY matchingNumber ASC");
$resulta->execute();
for($ia=0; $rowa = $resulta->fetch(); $ia++){
echo '<option value="'.$rowa['matchingNumber'].'">'.$rowa['matchingQuestion'].'</option>';
}	
echo '		</select>
</td>		
</tr>';
?>											
<label>Matching Choices</label>
<input type="text" name="matchingChoices" value="<?php echo $row['matchingChoices'];?>">				
<label>Matching Translation</label>
<input type="text" name="matchingChoicesTrans" value="<?php echo $row['matchingChoicesTrans'];?>">				
<label>Matching Points</label>
<input type="text" name="isMatch" value="<?php echo $row['isMatch'];?>">										
</div>
<div class="modal-footer">
<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
<button type="submit" name="submit" class="btn btn-danger">Yes</button>			
</div>
</form>
</div>
</div>
</div>
<?php } ?>
</tbody>
</table>



</div>
</div>
</div>
</div>


</body>
</html>


