<?php 
session_start();

include '../database/db.php';

if (isset($_POST)) {
	$acctName = $_POST['acctName']; 
	$acctEmail = $_POST['acctEmail']; 
	$acctPassword = md5($_POST['acctPassword']);
	//$acctPassword = hash('sha256',$_POST['acctPassword']);
	//$userType = $_POST['userType'];

	$query = "INSERT INTO account SET acctName=?, acctEmail=?, acctPassword=?, dateRegister=NOW()";	
	$stmt = $conn->prepare($query);
	$stmt -> bindParam(1,$acctName);
	$stmt -> bindParam(2,$acctEmail);
	$stmt -> bindParam(3,$acctPassword);
	//$stmt -> bindParam(3,$userType);
	//$stmt -> bindParam(4,$dateRegister);

	if ($stmt->execute()) {

		$_SESSION['name'] = $acctName;
		$_SESSION['email'] = $acctEmail;
		$_SESSION['password'] = $acctPassword;

		// GET ACCOUNT DETAILS OF THE USER
		$query2 = "SELECT * FROM account WHERE acctName=? AND acctPassword=?";
		$stmt2= $conn->prepare($query2);
		$stmt2 -> bindParam(1,$acctName);
		$stmt2 -> bindParam(2,$acctPassword);
				
	
		if ($stmt2->execute()) {
			$row = $stmt2->fetch();
			$id = $row['acctID'];
			$asd=$row['acctEmail'];
			$_SESSION['id'] = $id;
			$_SESSION['asd'] = $asd;


			//CREATE USES INFO
			$targetdir	=	"../uploads/userinfo/";
			$targetfile	=	$targetdir.basename($_FILES['userImage']['name']);
			$uploadOk	=	1;
			$imageType	=	pathinfo($targetfile,PATHINFO_EXTENSION);
			move_uploaded_file($_FILES['userImage']['tmp_name'],$targetfile);
			echo "The image is uploaded";
		
			try{
				
				$query	="INSERT INTO userinfo SET 
					acctID=?, 
					userImage=?, 
					userFirstname=?,
					userMiddlename=?,
					userLastname=?,
					userBlkno=?,
					userLotno=?,
					userStreet=?,
					userBarangay=?,
					userMunicipality=?,
					userProvince=?,
					userContactno=?,
					userBdate=?,
					userGender=?,
					userStatus=?
					";

				echo $targetfile;
				$stmt3	=	$conn->prepare($query);
				$stmt3	->	bindParam(1,$_SESSION['id']);
				$stmt3	->	bindParam(2,$targetfile);
				
				
				$stmt3	->	bindParam(3,$_POST['userFirstname']);
				$stmt3	->	bindParam(4,$_POST['userMiddlename']);
				$stmt3	->	bindParam(5,$_POST['userLastname']);
				$stmt3	->	bindParam(6,$_POST['userBlkno']);
				$stmt3	->	bindParam(7,$_POST['userLotno']);
				$stmt3	->	bindParam(8,$_POST['userStreet']);
				$stmt3	->	bindParam(9,$_POST['userBarangay']);
				$stmt3	->	bindParam(10,$_POST['userMunicipality']);
				$stmt3	->	bindParam(11,$_POST['userProvince']);
				$stmt3	->	bindParam(12,$_POST['userContactno']);
				$stmt3	->	bindParam(13,$_POST['userBdate']);
				$stmt3	->	bindParam(14,$_POST['userGender']);
				$status = 'PENDING';
				$stmt3	->	bindParam(15,$status);

				if ($stmt3	->	execute()){
					header('location:../unregister/successinfo.php');
				}else{
					//header('location:../unregister/failedinfo.php');
				}
			}catch (PDOException $e) {
				echo "ERROR ".$e->getMessage();}
		}else{
			echo "failed";
		}

// echo "<script>alert('You have successfully register account'); window.location='../account/createaccountform2.php'</script>";
//			header('location:successinfo.php');
		
	} else {
			echo "<script>alert('Failed to register account'); window.location='../account/createaccountform.php'</script>";
	}

	
} else {
	header('location:createaccountform.php');
}


?>

