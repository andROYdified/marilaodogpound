<body>
	<div class="log_reg">
		<a href="index.php">Register</a> | <a href="login.php">Login</a>
	</div>
<div class="form">
	<h2>Register</h2>
	<hr>
	<p id="msg"></p>
	<!--Form start here-->
	<form onsubmit="return false" method="post" id="register_form">
	<label>Name</label>
	<div class="input">
		<div>
			<input type="text" placeholder="Name" name="name" id="name">
		</div>
		<div class="error u_error"></div>
	</div>
	<label>Email</label>
	<div class="input">
		<div>
			<input type="email" placeholder="Email" name="u_email" id="u_email">
		</div>
		<div class="error e_error"></div>
	</div>	
	<label>Gender</label>
	<div class="input">
		<div style="margin-top:10px;">
			<input type="radio" name="gender" value="m">Male
			<input type="radio" name="gender" value="f">Female
		</div>
		<div class="error g_error"></div>
	</div>
	<label>Choose Country</label>
	<div class="input">
		<div>
			<select class="input-opt" name="u_country">
				<option value="">Choose Country</option>
				<option value="India">India</option>
				<option value="Pakistan">Pakistan</option>
				<option value="UAE">UAE</option>
				<option value="China">China</option>
				<option value="Afghanistan">Afghanistan</option>
			</select>
		</div>
		<div class="error"></div>
	</div>
	<label>Languages Known</label>
	<div class="input">
		<div style="margin-top:10px;">
			<input type="checkbox" name="lang[]" value="English">English
			<input type="checkbox" name="lang[]" value="Hindi">Hindi
			<input type="checkbox" name="lang[]" value="Urdu">Urdu
		</div>
		<div class="error"></div>
	</div>
	<label>Choose Password</label>
	<div class="input">
		<div>
			<input type="password" placeholder="Password" name="password" id="password">
		</div>
		<div class="error u_error"></div>
	</div>
	<label>Re-enter Password</label>
	<div class="input">
		<div>
			<input type="password" placeholder="Password" name="repassword" id="u_name">
		</div>
		<div class="error u_error"></div>
	</div>
	<div class="input">
		<div>
			<input type="submit" name="register" value="Register">
		</div>
	</div>
	</form>
</div>