<?php include('header.php');
//require_once('db.php');
include '../database/db.php';
?>
<body>

<div class="row-fluid">
<div class="span12">

<div class="container">

<?php //include ('modal_add.php'); ?>

<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
<div class="alert alert-info">
<button type="button" class="close" data-dismiss="alert">&times;</button>
<strong><i class="icon-user icon-large"></i>&nbsp;Dog Information </strong>
</div>
<thead>
<!-- <tr>
<th style="text-align:center;">DogID</th>-->
<th style="text-align:center;">#</th>
<th style="text-align:center;">Image</th>
<th style="text-align:center;">Dog's Name</th>

<th style="text-align:center;">Reason</th>
<th style="text-align:center;">Date Registered</th>
<th style="text-align:center;">Action</th>                                 
</thead>
<tbody>
<?php
require_once('db.php');
$sql='select * from dogfiles where status<=2';
$result = $conn->prepare($sql);
$result->execute();
for($i=0; $row = $result->fetch(); $i++){
$id=$row['id'];		
?>
<tr>
<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['id']; ?></td>
<td style="text-align:center; margin-top:10px; word-break:break-all; width:450px; line-height:100px;">
<?php 
$picture="../uploads/default.png";
if($row['picture'] != "" ) $picture='../uploads/'.$row['picture'];										
?>
<img src="<?php echo $picture; ?>" width="100px" height="100px" style="border:1px solid #333333;">
</td>

<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['dogname']; ?></td>			
<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['reason']; ?></td>
<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['dateregister']; ?></td>

<td style="text-align:center; width:350px;">
<a href="#updte_img<?php echo $id;?>"  data-toggle="modal"  class="btn btn-warning" >For Adoption</a>
</td>
</tr>
</div>
</div>
<!-- Modal Update Image -->
<div id="updte_img<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">			
<div class="modal-header"><h3 id="myModalLabel">Update</h3></div>
<div class="modal-body">


<form action="edit.php<?php echo '?id='.$id; ?>" method="post" enctype="multipart/form-data">		

<?php 
$picture="../uploads/default.png";
if($row['picture'] != "" ) $picture='../uploads/'.$row['picture'];										
?>
<img src="<?php echo $picture; ?>" width="100px" height="100px" style="border:1px solid #333333;">

<div style="color:blue; margin-left:150px; font-size:30px;">																			
<input 	type="hidden" name="status" value="3">	

<?php


$query = "SELECT * FROM questionmatching   order by matchingnumber ";
$stmt = $conn->prepare($query);
$stmt->execute();

foreach ($stmt->fetchall() as $ask=>$question) {

echo 'Part '.$question['4'].'. ';
echo $question['0'].'<br>';
echo $question['3'].'<br>';
$query2 = "SELECT * FROM choicesmatching where matchingnumber=".$question['0'];
$stmt2 = $conn->prepare($query2);
$stmt2->execute();
foreach ($stmt2->fetchall() as $answ=>$sagot) {

echo 
'<div>
<input type="radio" 
name="'.$question['0'].'" 
id="'.$sagot['0'].'" 
value="'.$sagot['0'].'"  required/>
<label>	'.$sagot['3'].' / 
'.$sagot['4'].'</label>
</div><br>';
}
echo '<br>';		
} ?>													

</div>
<div class="modal-footer">
<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
<button type="submit" name="submit" class="btn btn-danger">Yes</button>			
</div>
</form>
</div>
</div>
</div>
<?php } ?>
</tbody>
</table>



</div>
</div>
</div>
</div>


</body>
</html>


