<!-- Modal Update Image -->
<div id="updte_img<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">			
	<div class="modal-header"><h3 id="myModalLabel">Update</h3></div>
		<div class="modal-body">
			<div class="alert alert-danger">
		
			<form action="edit.php<?php echo '?id='.$id; ?>" method="post" enctype="multipart/form-data">		
					<label>Matching Question</label>
						<input type="text" name="matchingquestion" value="<?php echo $row['matchingQuestion'];?>">
					<label>Matching Translation</label>
						<input type="text" name="matchingtranslation" value="<?php echo $row['matchingTranslation'];?>">				
					<label>Doq Question</label>
						<input type="text" name="dogquestion" value="<?php echo $row['dogquestion'];?>">					
					<label>Question Part</label>
						<input type="text" name="qpart" value="<?php echo $row['qpart'];?>">					
				</div>
			<div class="modal-footer">
				<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
				<button type="submit" name="submit" class="btn btn-danger">Yes</button>			
			</div>
			</form>
		</div>
	</div>
</div>              
