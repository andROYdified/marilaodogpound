<?php

include '../template/headeruser.php';

?>
<!DOCTYPE html>
<html>
<head>
	<title>Adopt</title>
</head>
<style>
	body{
	font-family:arial;
	font-size:15px;
	/*line-height: 1.6em;*/
}
li{
	list-style: none;
}
a{
	text-decoration: none;
}
label{
	display: inline-block;
	width: 180px;
}
input[type='text']{
	width: 97%;
	padding: 4px;
	border-radius: 5px;
	/*border:1px #ccc solid;*/
}
input[type='number']{
	width: 50px;
	padding: 4px;
	border-radius: 5px;
	border:1px #ccc solid;
}
.container{
	width: 60%;
	margin:0 auto;
	overflow:auto;
}
header{
	border-bottom:3px #f4f4f4 solid;
}
footer{
	border-top: 3px #f4f4f4 solid;
	text-align: center;
	padding-top: 5px;
}
main{
	padding-bottom: 20px;
}
a.start{
	display: inline-block;
	color: #1B5E20;
	background-color: #00C853;
	border:1px dotted #000;
	padding:6px 13px;
}
.current{
	padding: 10px;
	background: #f4f4f4;
	border: #ccc dotted 1px;
	margin: 20px 0 10px 0;
}
@media only screen and (max-width: 960px){
	.container{
		width: 80%;
	}
}

/*adoption information*/
* {
    box-sizing: border-box;
}

body {
    margin: 0;
    font-family: Arial;
    font-size: 17px;
}

.container-AdoptInfo {
    position: relative;
    max-width: 700px;
    margin-left: 10px;
}

.container-AdoptInfo img {vertical-align: middle;}

.container-AdoptInfo .content-AdoptInfo {
    position: absolute;
    bottom: 0;
    background: rgba(0, 0, 0, 0.5); /* Black background with transparency */
    color: #f1f1f1;
    width: 670px;
    padding: 10px;
}
.qualification{
	margin-left: 10px;

}
/*-----------------------CARDS RIGHT------------------------------*/
.card-Adopt {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  width: 400px;
  height: 460px;
  margin: auto;

  font-family: arial;
}

.title-Adopt {
  color: grey;
  font-size: 18px;
}

.Test button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 18px;
}

a {
  text-decoration: none;
  font-size: 22px;
  color: black;
}

.Test button:hover, a:hover {
  opacity: 0.7;
}

</style>
<body style="background-color: #FFECB3">

 <div class="container-AdoptInfo col-sm-8" style="margin-left: 110px">
  <img src="../DogPhotos/DogBackImage3.jpg" alt="DogBackImage3" style="width:100%; height: 70%">
  <div class="content-AdoptInfo">
   <center> <h1>Why Adoption?</h1>
    <p>Every dog deserves a new home and a new family if you struggle to find a new adopted dog then take our qualification and matching test that helps you to find a dog that matches in your characteristics. Let these dogs have a chance to live in a family where they can be loved and cared again, do not forget them and do not close your heart. They deserve to be loved give them a new home. </p></center>
  </div>
</div>

<div class="card-Adopt col-sm-4" style="text-align: center;">

  <img src="../weenvision/DogPhoto30.jpg" alt="DogPhoto30" style="width:350px; height: 200px">
  <h3>ADOPTION QUALIFICATION TEST</h3>
 	<h4><h4>Please read and answer the following questions carefully and honestly.<br>
	This test contains set of questions pertaining to your personal information<br>
	and questions pertaining to ideal dog that you wanted to adopt</h4></h4>
<a href="../Quiz/index.php"><input class="btn btn-warning" type="submit" value="Take a Test"></a>
</div>


  
 
<!--div class= "col-sm-4" style="text-align: center">
  <div class="qualification">
<center><h2>ADOPTION QUALIFICATION TEST</h2>
<br>

<h4>Please read and answer the following questions carefully and honestly.<br>
This test contains set of questions pertaining to your personal information<br>
and questions pertaining to ideal dog that you wanted to adopt</h4>
<br>
<br>
<a href="../Quiz/index.php"><input class="btn btn-warning" type="submit" value="Take a Test"></a>
</center-->


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

</body>
</html>

<?php include '../template/footer.php'; ?>