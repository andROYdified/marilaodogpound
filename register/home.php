<?php
//include '../template/headeruser.php';

include '../template/homeuser.php';
include '../template/news.php'; 

require_once('../database/db.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Home User</title>
</head>
<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
<body style="background-color: #FFECB3">


<!--div class="row" style="margin-left: 30px">
     <div class="col-sm-3" style="text-align: center">
   <img class="img-circle" src="../uploads/news/news1.jpg" style="width: 200px; height: 200px">
      
      <h4>Da-BAI to hold animal welfare kick-off celebration</h4>
         <a href="../template/eventsinfo.php"><input type="button" class="btn btn-warning" name="" value="Read More"></a>
    </div>
   <div class="col-sm-3" style="text-align: center">
   <img class="img-circle" src="../uploads/news/news2.jpg" style="width: 200px; height: 200px">
      <h4>BAI to Spearhead the World Rabies Day Celebration</h4>
        <a href="../template/eventsinfo.php">  <input type="button" class="btn btn-warning" name="" value="Read More"></a>
    </div>
    <div class="col-sm-3" style="text-align: center">
      <img class="img-circle"  src="../uploads/news/news3.jpg" style="width: 200px; height: 200px">
      
      <h4>NRPCC to Conduct Rabies Awareness Month Kick-off Activity</h4>
         <a href="../template/eventsinfo.php"><input type="button" class="btn btn-warning" name="" value="Read More"></a>
    </div>
    <div class="col-sm-3" style="text-align: center">
      <a href="../template/eventsinfo.php"><img class="img-circle" src="../uploads/news/news4.jpg" style="width: 200px; height: 200px"></a>
      <h4>DA-BAI to Hold World Rabies Day and Animal Welfare Kick-Off Celebration</h4>
          <a href="../template/eventsinfo.php"><input type="button" class="btn btn-warning" name="" value="Read More"></a>
    </div>
  
</div-->



<!--<style>
* {box-sizing: border-box}
body {font-family: Verdana, sans-serif; margin:0}
.mySlides {display: none}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.text-caption {
  color: black;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
/*.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}*/

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

/*@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}*/

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .prev, .next,.text {font-size: 11px}
}
/*----NEWS CONTENT*/
* {
    box-sizing: border-box;
}

body {
    margin: 0;
    font-family: Arial;
    font-size: 17px;
}

.container-NewsInfo {
    position: relative;
    max-width: 800px;
    margin: 0 auto;
}



.container-NewsInfo .content-NewsInfo {
    position: absolute;
    bottom: 0;
    background: rgba(0, 0, 0, 0.5); /* Black background with transparency */
    color: #f1f1f1;
    width: 100%;
    padding: 20px;
}


</style>
</head>
<body>

<div class="slideshow-container">

<div class="mySlides fade">
  <div class="numbertext">1 / 4</div>
  <img src="../uploads/news/news1.jpg" style="width:100%">
  <div class="text-caption">
  <div class="container-NewsInfo">
  <div class="content-NewsInfo">
  <h3>Da-BAI to hold animal welfare kick-off celebration</h3>
  </div>
  </div>
</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">2 / 4</div>
  <img src="../uploads/news/news2.jpg" style="width:100%">
   <div class="text-caption">
  <div class="container-NewsInfo">
  <div class="content-NewsInfo">
  <h3>BAI to Spearhead the World Rabies Day Celebration</h3></div>
</div>
</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">3 / 4</div>
  <img src="../uploads/news/news3.jpg" style="width:100%">
  <div class="text-caption">
  <div class="container-NewsInfo">
  <div class="content-NewsInfo">
  <h3>NRPCC to Conduct Rabies Awareness Month Kick-off Activity</h3></div>
  </div>
  </div>
</div>

<div class="mySlides fade">
  <div class="numbertext">4 / 4</div>
  <img src="../uploads/news/news3.jpg" style="width:100%">
  <div class="text-caption">
  <div class="container-NewsInfo">
  <div class="content-NewsInfo">
  <h3>DA-BAI to Hold World Rabies Day and Animal Welfare Kick-Off Celebration</h3>
  </div>
  </div>
  </div>
</div>



<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
<a class="next" onclick="plusSlides(1)">&#10095;</a>

</div>
<br>

<div style="text-align:center">
  <span class="dot" onclick="currentSlide(1)"></span> 
  <span class="dot" onclick="currentSlide(2)"></span> 
  <span class="dot" onclick="currentSlide(3)"></span> 
  <span class="dot" onclick="currentSlide(4)"></span> 
</div>

<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}
</script>-->






<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script
  src="http://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

 <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 80%;
      height: 80%;
      margin: auto;
       background-color: #5D4037;
  }
  .container-news{
    background-color: #5D4037;
    height: 100%;
  }
  .role{
    background-color: #5D4037;
  }

  </style>
</head>
<body>


<div class="container-news">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">

      <?php
            $result = $conn->prepare("SELECT * FROM eventlist WHERE pinnedPost = 1 ORDER BY ID ASC ");
            $result->execute();
            $xrow = $result->fetchall();	

            foreach($xrow as $row):
      ?>
      
        <div class="item <?php if($row['id'] == $xrow[0]['id']) echo 'active'; ?>">
        <img src="../uploads/<?=$row['picture'] ?>" alt="<?= $row['eventName'] ?>">
        <div class="carousel-caption">
        <br>
        <br>
          <h3><?= $row['eventName'] ?></h3>
         <a class="btn btn-warning" href="./eventsinfo.php?eventID=<?= $row['id']?>">Read More</a>
        </div>
      </div>
      <?php endforeach; ?>
      
      <ol class="carousel-indicators">
        <?php for($i = 0; $i < count($xrow); $i++): ?>
          <li data-target="#myCarousel" data-slide-to="<?= $i ?>" <?php if($i==0) echo "class='active'"; ?>></li>
        <?php endfor; ?>
      </ol>
      <!-- <div class="item active">
        <img src="../uploads/news/news1.jpg" alt="Chania">
        <div class="carousel-caption">
        <br>
        <br>
          <h3>Da-BAI to hold animal welfare kick-off celebration</h3>
         <input type="button" style="background-color: #F57F17;padding: 15px;width: 150px;text-decoration: none;color: white;font-weight: bold;border-style: none" class="readmore" name="" value="Read More">
        </div>
      </div>

      <div class="item">
        <img src="../uploads/news/news2.jpg" alt="Chania">
        <div class="carousel-caption">
          <h3>BAI to Spearhead the World Rabies Day Celebration</h3>
           <input type="button" style="background-color: #F57F17;padding: 15px;width: 150px;text-decoration: none;color: white;font-weight: bold;border-style: none" class="readmore" name="" value="Read More">
        </div>
      </div>
    
      <div class="item">
        <img src="../uploads/news/news3.jpg" alt="Flower">
        <div class="carousel-caption">
          <h3>NRPCC to Conduct Rabies Awareness Month Kick-off Activity</h3>
           <input type="button" style="background-color: #F57F17;padding: 15px;width: 150px;text-decoration: none;color: white;font-weight: bold;border-style: none" class="readmore" name="" value="Read More">
        </div>
      </div>

      <div class="item">
        <img src="../uploads/news/news2.jpg" alt="Flower">
        <div class="carousel-caption">
          <h3>DA-BAI ccc to Hold World Rabies Day and Animal Welfare Kick-Off Celebration</h3>
           <input type="button" style="background-color: #F57F17;padding: 15px;width: 150px;text-decoration: none;color: white;font-weight: bold;border-style: none" class="readmore" name="" value="Read More">
        </div>
      </div> -->
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
</body>
</html>
<br>
<br>
<?php include '../template/footer.php'?>