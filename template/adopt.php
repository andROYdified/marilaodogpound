<?php



?>
<!DOCTYPE html>
<html>
<head>
	<title>Adopt</title>
</head>
<style>
	body{
	font-family:arial;
	font-size:15px;
	line-height: 1.6em;
}
li{
	list-style: none;
}
a{
	text-decoration: none;
}
label{
	display: inline-block;
	width: 180px;
}
input[type='text']{
	width: 97%;
	padding: 4px;
	border-radius: 5px;
	border:1px #ccc solid;
}
input[type='number']{
	width: 50px;
	padding: 4px;
	border-radius: 5px;
	border:1px #ccc solid;
}
.container{
	width: 60%;
	margin:0 auto;
	overflow:auto;
}
header{
	border-bottom:3px #f4f4f4 solid;
}
footer{
	border-top: 3px #f4f4f4 solid;
	text-align: center;
	padding-top: 5px;
}
main{
	padding-bottom: 20px;
}
a.start{
	display: inline-block;
	color: #1B5E20;
	background-color: #00C853;
	border:1px dotted #000;
	padding:6px 13px;
}
.current{
	padding: 10px;
	background: #f4f4f4;
	border: #ccc dotted 1px;
	margin: 20px 0 10px 0;
}
@media only screen and (max-width: 960px){
	.container{
		width: 80%;
	}
}

</style>
<body style="background-color: #FFECB3">
<center><h2>ADOPTION QUALIFICATION TEST</h2>
<br>
<br>
<h4>Please read and answer the following questions carefully and honestly.<br>
This test contains set of questions pertaining to your personal information<br>
and questions pertaining to ideal dog that you wanted to adopt</h4>
<br>
<br>
</center>

<!--<div class="container">
			<div class="current" style="background-color: FAFAD2"><h3><b>Question</b></h3> 
		<h3><b>1.The kind of Building you live in?</b></h3>
		<h4><i>Anong uri ng tahanan ang iyong tinitirahan?</i></h4>
		<ul class="choices" style="background-color: FAFAD2">
		<li><input name="choice" type="radio" value="" />Town</li>	
		<br>
		<li><input name="choice" type="radio" value="" />Town House/Apartment/Condo</li>
				
			</ul>
			</div>
			<br>

		<h3><b>2.Do you own your house?</b></h3>
		<h4><i>Ikaw ba ang nagmamay-ari ng iyong bahay?</i></h4>
		<ul class="choices" style="background-color: FAFAD2">
		<li><input name="choice" type="radio" value="" />Yes/<i>Oo</i></li>	
		</ul>
		<br>
		<li><input name="choice" type="radio" value="" />No/<i>Hindi</i></li>
		
		<br>

		<h3><b>3.If you rent your house, does your landlord allows a pet, especially a dog?</b></h3>
		<h4><i>Kung ang iyong tirahan ay nirerentahan mo, ang nagmamay-ari ba nito ay pinapayagan mag-alaga ng alagang aso?</i></h4>
		<ul class="choices" style="background-color: FAFAD2">
		<li><input name="choice" type="radio" value="" />Yes/<i>Oo</i></li>	
		<br>
		<li><input name="choice" type="radio" value="" />No/<i>Hindi</i></li>
		</ul>
		<br>

		<h3><b>4.Are you the only person who lives in your house?</b></h3>
		<h4><i>Ikaw lang ba ang tanging nakatira sa inyong bahay?</i></h4>
		<ul class="choices" style="background-color: FAFAD2">
		<li><input name="choice" type="radio" value="" />Yes/<i>Oo</i></li>	
		<br>
		<li><input name="choice" type="radio" value="" />No/<i>Hindi</i></li>
		</ul>
		<br>

		<h3><b>5.Describe your household Activity Level</b></h3>
		<h4><i>Maari mo bang ilarawan kung anong lebel ng aktibidad mayroon ang inyong tirahan??</i></h4>
		<ul class="choices" style="background-color: FAFAD2">
		<li><input name="choice" type="radio" value="" />PeaceFul/<i></i></li>	
		<br>
		<li><input name="choice" type="radio" value="" />Loud/<i></i></li>
		<br>
		<li><input name="choice" type="radio" value="" />Average/<i></i></li>
		</ul>
		<br>

		<h3><b>6.Are there any regular visitors in your home?</b></h3>
		<h4><i>Mayroon bang madalas na bumibisita sa inyong tahanan?</i></h4>
		<ul class="choices" style="background-color: FAFAD2">
		<li><input name="choice" type="radio" value="" />Yes/<i>Oo</i></li>	
		<br>
		<li><input name="choice" type="radio" value="" />No/<i>Hindi</i></li>
		</ul>
		<br>

		<h3><b>7.	
If yes, who are the usual visitors in your home?</b></h3>
		<h4><i>Kung Oo, sinu-sino ang mga madalas na bumibisita sa iyong tahanan?</i></h4>
		<ul class="choices" style="background-color: FAFAD2">
		<li><input name="choice" type="radio" value="" />Children/<i></i></li>	
		<br>
		<li><input name="choice" type="radio" value="" />Teenagers/<i></i></li>
		<br>
		<li><input name="choice" type="radio" value="" />Adult/<i></i></li>
		<br>
		<li><input name="choice" type="radio" value="" />Senior/<i></i></li>
		</ul>
		<br>

		<h3><b>8.Part of your house where the dog will stay(During Daytime)</b></h3>
		<h4><i>Parte ng inyong bahay kung saan mananatiling alagang aso(Tuwing Umaga)</i></h4>
		<ul class="choices" style="background-color: FAFAD2">
		<li><input name="choice" type="radio" value="" />Inside/<i>Nasa loob</i></li>	
		<br>
		<li><input name="choice" type="radio" value="" />Outside/<i>Nasa labas</i></li>
		<br>
			<li><input name="choice" type="radio" value="" />Both/<i>Parehas</i></li>
		</ul>
		<br>

		<h3><b>9.Part of your house where the dog will stay(During Night time)</b></h3>
		<h4><i>Parte ng inyong bahay kung saan mananatiling alagang aso(Tuwing Gabi)</i></h4>
		<ul class="choices" style="background-color: FAFAD2">
		<li><input name="choice" type="radio" value="" />Inside/<i>Nasa loob</i></li>	
		<br>
		<li><input name="choice" type="radio" value="" />Outside/<i>Nasa labas</i></li>
		<br>
			<li><input name="choice" type="radio" value="" />Both/<i>Parehas</i></li>
		</ul>
		<br>

		<h3><b>10.Does your home a fence?</b></h3>
		<h4><i>Mayroon ba kayong Bakod ang inyong tirahan?</i></h4>
		<ul class="choices" style="background-color: FAFAD2">
		<li><input name="choice" type="radio" value="" />Yes/<i>Oo</i></li>	
		<br>
		<li><input name="choice" type="radio" value="" />No/<i>Hindi</i></li>
		</ul>
		<br>

		<h3><b>11.Where does your monthly income came from?</b></h3>
		<h4><i>Saan nagmula ang iyong Kabuwanang Pinagkakakitaan?</i></h4>
		<ul class="choices" style="background-color: FAFAD2">
		<li><input name="choice" type="radio" value="" />Business/<i></i></li>	
		<br>
		<li><input name="choice" type="radio" value="" />Employed/<i></i></li>
		</ul>
		<li><input name="choice" type="radio" value="" />Self-Employed/<i></i></li>
		</ul>
		<br>
		<li><input name="choice" type="radio" value="" />Part time/<i></i></li>
		</ul>
		<br>

		<h3><b>12.Number of workdays in one week?</b></h3>
		<h4><i>Bilang ng araw ng pagtatrabaho mo sa isang?</i></h4>
		<ul class="choices" style="background-color: FAFAD2">
		<li><input name="choice" type="radio" value="" />2-3 days/<i></i></li>	
		<br>
		<li><input name="choice" type="radio" value="" />4-5 days/<i></i></li>
		<br>
		<li><input name="choice" type="radio" value="" />6 days/<i></i></li>
		</ul>
		<br>

		<h3><b>13.Number of working hours in one day?</b></h3>
		<h4><i>Ilang oras ka nagtratrabaho sa loob ng isang araw?</i></h4>
		<ul class="choices" style="background-color: FAFAD2">
		<li><input name="choice" type="radio" value="" />5 hours- 7 hours/<i></i></li>	
		<br>
		<li><input name="choice" type="radio" value="" />8 hours- 9 hours/<i></i></li>
		<br>
		<li><input name="choice" type="radio" value="" />10 hours and above/<i></i></li>
		</ul>
		<br>

		<h3><b>14.What is your estimated monthly income?</b></h3>
		<h4><i>Estimasyon ng iyong kabuwanang kita?</i></h4>
		<ul class="choices" style="background-color: FAFAD2">
		<li><input name="choice" type="radio" value="" />P10,000-P15,000/<i></i></li>	
		<br>
		<li><input name="choice" type="radio" value="" />P16,000-P20,000/<i></i></li>
		<br>
		<li><input name="choice" type="radio" value="" />21,000 and above/<i></i></li>
		</ul>
		<br>












				<input class="btn btn-primary" type="submit" value="Submit">
</div>-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<title>PHP Quiz</title>
	
	<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body>

	<!--<div id="page-wrap">-->

		<h1>Final Quiz for Lip building</h1>
		
		<form action="../Quiz/grade.php" method="post" id="quiz">
		
            <ol>
            
                <li>
                
                    <h3>CSS Stands for...</h3>
                    
                    <div>
                        <input type="radio" name="question-1-answers" id="question-1-answers-A" value="A" />
                        <label for="question-1-answers-A">A) Computer Styled Sections </label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-1-answers" id="question-1-answers-B" value="B" />
                        <label for="question-1-answers-B">B) Cascading Style Sheets</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-1-answers" id="question-1-answers-C" value="C" />
                        <label for="question-1-answers-C">C) Crazy Solid Shapes</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-1-answers" id="question-1-answers-D" value="D" />
                        <label for="question-1-answers-D">D) None of the above</label>
                    </div>
                
                </li>
                
                <li>
                
                    <h3>Internet Explorer 6 was released in...</h3>
                    
                    <div>
                        <input type="radio" name="question-2-answers" id="question-2-answers-A" value="A" />
                        <label for="question-2-answers-A">A) 2001</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-2-answers" id="question-2-answers-B" value="B" />
                        <label for="question-2-answers-B">B) 1998</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-2-answers" id="question-2-answers-C" value="C" />
                        <label for="question-2-answers-C">C) 2006</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-2-answers" id="question-2-answers-D" value="D" />
                        <label for="question-2-answers-D">D) 2003</label>
                    </div>
                
                </li>
                
                <li>
                
                    <h3>SEO Stand for...</h3>
                    
                    <div>
                        <input type="radio" name="question-3-answers" id="question-3-answers-A" value="A" />
                        <label for="question-3-answers-A">A) Secret Enterprise Organizations</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-3-answers" id="question-3-answers-B" value="B" />
                        <label for="question-3-answers-B">B) Special Endowment Opportunity</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-3-answers" id="question-3-answers-C" value="C" />
                        <label for="question-3-answers-C">C) Search Engine Optimization</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-3-answers" id="question-3-answers-D" value="D" />
                        <label for="question-3-answers-D">D) Seals End Olives</label>
                    </div>
                
                </li>
                
                <li>
                
                    <h3>A 404 Error...</h3>
                    
                    <div>
                        <input type="radio" name="question-4-answers" id="question-4-answers-A" value="A" />
                        <label for="question-4-answers-A">A) is an HTTP Status Code meaning Page Not Found</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-4-answers" id="question-4-answers-B" value="B" />
                        <label for="question-4-answers-B">B) is a good excuse for a clever design</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-4-answers" id="question-4-answers-C" value="C" />
                        <label for="question-4-answers-C">C) should be monitored for in web analytics</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-4-answers" id="question-4-answers-D" value="D" />
                        <label for="question-4-answers-D">D) All of the above</label>
                    </div>
                
                </li>
                
                <li>
                
                    <h3>Your favorite website is</h3>
                    
                    <div>
                        <input type="radio" name="question-5-answers" id="question-5-answers-A" value="A" />
                        <label for="question-5-answers-A">A) CSS-Tricks</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-5-answers" id="question-5-answers-B" value="B" />
                        <label for="question-5-answers-B">B) CSS-Tricks</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-5-answers" id="question-5-answers-C" value="C" />
                        <label for="question-5-answers-C">C) CSS-Tricks</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-5-answers" id="question-5-answers-D" value="D" />
                        <label for="question-5-answers-D">D) CSS-Tricks</label>
                    </div>
                
                </li>
            
            </ol>
            
            <input type="submit" value="Submit Quiz" />
		
		</form>
	
	</div>
	
	<script type="text/javascript">
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript">
	var pageTracker = _gat._getTracker("UA-68528-29");
	pageTracker._initData();
	pageTracker._trackPageview();
	</script>

</body>

</html>
