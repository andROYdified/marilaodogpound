<?php

?>
<style type="text/css">
  /* Container holding the image and the text */
.container {
    position: relative;
    text-align: center;
    color: white;
}

/* Bottom left text */
.bottom-left {
    position: absolute;
    bottom: 8px;
    left: 16px;
}

/* Top left text */
.center-left {
    position: absolute;
    top: 550px;
    left: 20px;
}

/* Top right text */
.top-right {
    position: absolute;
    top: 8px;
    right: 16px;
}

/* Bottom right text */
.bottom-right {
    position: absolute;
    bottom: 8px;
    right: 16px;
}

/* Centered text */
.centered {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}
.statistics{
  border:1px solid gray;
  box-shadow:0px 0px 10px gray; 
  padding: 15px;
  height: 150px;
  background-color: orange;


}
.readmore a:hover {
  color: #3E2723;

  background-color: #A1887F;
}
.container-home img{
  width: 100%;
}

</style>
<!--div class="row" >
<img class="blackdog2" src="../webbackground/DogPhoto29.jpg" style="width:100%; height: 90%"-->
<!--888888888888888888START OF CAROUSEL8888888888888888888-->

 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-home">
    
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="../DogPhotos/DogPhoto29.jpg" alt="Los Angeles" style="width:100%; height: 90%">
        <div class="carousel-caption">
          <h2>Give them a chance to be loved by opening your home for them</h2>
          
        </div>
      </div>

      <div class="item">
        <img src="../DogPhotos/DogPhoto39.jpg" alt="Chicago" style="width:100%; height: 90%">
        <div class="carousel-caption">
          <h2> Let your help change a stray dog's life into becoming a pet dog tomorrow</h2>
          
        </div>
      </div>
    
      <div class="item">
        <img src="../DogPhotos/DogPhoto74.jpg" alt="New york" style="width:100%; height: 90%">
        <div class="carousel-caption">
          <h2>Let your love be their greatest hope and let your home be their new family</h2>
          
        </div>
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

<!--888888888888888888END OF CAROUSEL8888888888888888888-->
 <!--div class="center-left" style="color:white"><h2>Give them a chance to be loved by opening your home for them</h2><h4>Let your love be their greatest hope and let your home be their new family</h4>



    <div>
      <a style="color: white" href="../unregister/about.php"><input type="submit" class="" name="submit" value="Read More" style="width: 150px; height: 50px; margin-left: 150px; background-color: #F57F17;font-size: 20px; border: none;"></a>
    </div>-->
 
 
  </div>
 </div>

 

  </div>
<br>
<br>
<br>

  <div class="row">
     <div class="col-sm-12">
     <center>
      <h1 style="color: #5D4037">We Provide Best Services for your dog <br></h1><p style="margin-left: 150px;margin-right: 150px" ><h4>We envision a greater change for the dogs and for the people in the surroundings by offering our services you. </h4>
      </center>
      <br>
      <br>
    </div>

  <div class="row" style="margin-left: 30px">
    
    <div class="col-sm-4" style="text-align: center">
   <img src="../webicons/Online_Reg2.png" style="width: 150px; height: 150px" onclick="alert('You must log in first to register a dog');">
      <h3 style="color: #5D4037" onclick="alert('You must log in first to register a dog');">Register your Dog</h3>
      <h4 style="color: #3E2723">MDP offers a convinient way to register your dog.</h4>
      <input type="button" style="background-color: #F57F17;padding: 15px;width: 150px;text-decoration: none;color: white;font-weight: bold;border-style: none" class="readmore" name="" value="Read More">
    </div>

    <div class="col-sm-4" style="text-align: center">
     
     <img src="../webicons/Dog_Adoption.png" style="width: 150px; height: 150px" onclick="alert('You must log in first to adopt a dog');"><h3 style="color: #5D4037" onclick="alert('You must log in first to adopt a dog');">Share the love and Adopt a Dog</h3>
      <h4 style="color: #3E2723">Open your home and your heart</h4>
       <input type="button" style="background-color: #F57F17;padding: 15px;width: 150px;text-decoration: none;color: white;font-weight: bold;border-style: none" class="readmore" name="" value="Read More">
    </div>

    <div class="col-sm-4" style="text-align: center">
    
    <img src="../webicons/Dog_Report.png" style="width: 150px; height: 150px" onclick="alert('You must log in first to send a dog report');"><h3 style="color: #5D4037" onclick="alert('You must log in first to send a dog report');">Send your report here </h3></a> 
      <h4 style="color: #3E2723"  >We want to hear your Dog Reports</h4>
       <a href="../unregister/services.php"><input type="button" style="background-color: #F57F17;padding: 15px;width: 150px;text-decoration: none;color: white;font-weight: bold;font-size: 15px;border-style: none" class="readmore" name="" value="Read More"></a>
    </div>
  </div>
</div>
  <br>
  <br>
  <br>
 <!-- <div class="container">
     <div class="jumbotron" style="background-color: orange">
     <center>
     <br>
      <h1 style="color: 8A2BE2">Marilao Dog Pound has an estimated No. of Dogs 25, 000</h1>
      </center>
    </div>
      
      
    </div>-->
</div>
  
<style>
div.img {
     margin: 10px;
    border: 1px solid blue;
     float: left;
     width: 180px;
 }

div.img:hover {
     border: 1px solid #777;
 }

div.img img {
     width: 100%;
    height: auto;
 }

div.desc {
    padding: 15px;
     text-align: center;
 }

</style>
