<?php

  require_once('../database/db.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Marilao Dog Events</title>

  <style>

  </style>
</head>
<body style="background-color: #FFECB3">
<!--<div class="container col-sm-12" >
<img src="../webbackground/DogPhoto28.jpg" style="width:100%;height: 80%">
 <div class="center-left">-->

<!-- <div class="input">
    <div>
      <input type="submit" class="btn btn-warning" name="submit" value="Read More" style="width: 150px; height: 50px; margin-left: 150px">
    </div>
  </div>
 </div>-->
<!--</div>-->

  <div class="container">
    <div class="jumbotron" style="background-color: #FFECB3">
      <center><h1 style="color: #5D4037">Marilao Dog Events</h1>
      <h3>Get updated on the latest events in Marilao Dog Pound by checking it out here.</h3>
      <br>
      <h6><p>Being updated helps you to become aware on the latest events and news that is happening around you, especially when it comes to latest updates for the welfare of your pet dog. It is important that there is always be an information spread to people because this would give them knowledge, that is why one of the fast way to disseminate latest updates for dogs is through the use of social media like a website. Marilao Dog Pound aims to help especially the Marilao citizens to become aware on the latest updates about dog by providing a page for events.
    What we aim is to make everyone be updated, give them the information they need, and help them to become aware especially the information that they can learn about dogs. To get to know more the latest news and updates all of these are categorized by dates while the previous ones are at the lower part but still you can scroll it down to read updates that you never read before. 

    
      </p></h6>
      </center>
  

    </div>
  <br>
  <br>

  <div>
    <table class="table table-hover">
      <thead>
          <tr rowspan="2">
              <h2>Events</h2>
          </tr>        
      </thead>
      <tbody>
      <?php
            $result = $conn->prepare("SELECT * FROM eventlist ORDER BY ID ASC ");
            $result->execute();
            $xrow = $result->fetchall();	

            foreach($xrow as $row):
      ?>
          <tr>
              <td class="col-sm-3" style="text-align: center">
              <img class="img-square" src="../uploads/<?= $row['picture'] ?>" style="width: 150px; height: 150px">
              </td>
              <td class="col-sm-9">
                <h4><?= $row['eventName'] ?></h4>
                <br>
                <?php
                    $out = strlen($row['eventDesc'])  > 150 ? substr($row['eventDesc'], 0, 150)."..." : $row['eventDesc'];

                    echo $out;
                ?>
                <a href="./eventsinfo.php?eventID=<?= $row['id']?>" class="btn btn-link">Read More >>></a>
              </td>
          </tr>
            <?php endforeach; ?>
      </tbody>
    </table>
  </div>
  <!-- <div class="row" style="margin-left: 30px">
      <div class="col-sm-3" style="text-align: center">
    <img class="img-circle" src="../uploads/news/news1.jpg" style="width: 200px; height: 200px">
        
        <h4>Da-BAI to hold animal welfare kick-off celebration</h4>
          <a href="../template/eventsinfo.php"><input type="button" class="btn btn-warning" name="" value="Read More"></a>
      </div>
    <div class="col-sm-3" style="text-align: center">
    <img class="img-circle" src="../uploads/news/news2.jpg" style="width: 200px; height: 200px">
        <h4>BAI to Spearhead the World Rabies Day Celebration</h4>
          <a href="../template/eventsinfo.php">  <input type="button" class="btn btn-warning" name="" value="Read More"></a>
      </div>
      <div class="col-sm-3" style="text-align: center">
        <img class="img-circle"  src="../uploads/news/news3.jpg" style="width: 200px; height: 200px">
        
        <h4>NRPCC to Conduct Rabies Awareness Month Kick-off Activity</h4>
          <a href="../template/eventsinfo.php"><input type="button" class="btn btn-warning" name="" value="Read More"></a>
      </div>
      <div class="col-sm-3" style="text-align: center">
        <a href="../template/eventsinfo.php"><img class="img-circle" src="../uploads/news/news4.jpg" style="width: 200px; height: 200px"></a>
        <h4>DA-BAI to Hold World Rabies Day and Animal Welfare Kick-Off Celebration</h4>
            <a href="../template/eventsinfo.php"><input type="button" class="btn btn-warning" name="" value="Read More"></a>
      </div>
    
  </div> -->






  <?php include '../template/footer.php'?>

  </body>
</html>