
<body style="background-color: #FFECB3">
<div class="container" >
  <!-- <img src="../webbackground/DogPhoto28.jpg" style="width:100%;height: 80%">
  <div class="center-left"> -->


  <!--<div class="col-sm-12" style="margin-left: 30px">-->
  <br>
  <br>


  <?php

    require_once('../database/db.php');

    if(!isset($_REQUEST['eventID']))
        header('location: errorpage.php');

      
      $eventID = $_REQUEST['eventID'];

      $result = $conn->prepare("SELECT
          e.*, u.acctID, u.userFirstname, u.userLastname
        FROM eventlist e
        INNER JOIN userinfo u ON e.reportby = u.acctID WHERE id = $eventID ORDER BY ID ASC ");
      $result->execute();
      $xrow = $result->fetchall();	
  ?>

  <div class="col-sm-12">
    <center>
    <img class="img-fluid" src="../uploads/<?= $xrow[0]['picture'] ?>"  style="max-width: 100%; height: auto;">
    </center>
  </div>

  <div class="col-sm-12 text-center">
    <h3><?= $xrow[0]['eventName'] ?> <br><br> <small>By: <?= $xrow[0]['userFirstname'] . ' ' . $xrow[0]['userLastname'] ?></small></h3>
  </div>

  <div class="col-sm-12 text-justify">
    <?= $xrow[0]['eventDesc'] ?>
  </div>
</div>
<!-- 


  <div class="col-sm-3" style="">
     <img src="../uploads/news/news1.jpg" style="width: 400px; height: 300px;margin-top: 60px">
    </div>

    <div class="col-sm-8" style="margin-left: 100px">
 
   <center><h3>Da-BAI to hold animal welfare kick-off celebration</h3>By: Mark Francis S. España</center> 
    <br>
  <h4>The Department of Agriculture-Bureau of Animal Industry (DA-BAI) will hold the Animal Welfare Kick-Off Celebration on October 4, 2016 at the BAI Compound, Visayas Avenue, Diliman, Quezon City.
With this year’s theme "Animal Welfare is Human Welfare," the activity emphasizes the importance of collaborative efforts in promoting animal welfare and preventing animal cruelty in the Philippines. 
The activity will highlight the recognition of K9 Hero Dogs under the Armed Forces of the Philippines (AFP)- Veterinary Dispensary Corps.
<br><br>
DA Assistant Secretary for Livestock Dr. Enrico P. Garzon, Jr. and DA-Committee on Animal Welfare (CAW) Chairperson Atty. Ernesto A. Tabujara III  are expected to attend the awarding together with officials from DA-BAI, non-government organizations (NGOs), and private sector.
<br><br>
On the other hand, Ms. Korina Sanchez-Roxas, Mr. Coco Martin,  Former BAI Director Dr. Rubina O. Cresencio, DA-CAW members, Regional Animal Welfare Officers (RAWOs) and attached animal welfare institutions will be given Plaque of Recognition for their exemplary contribution in promotion and implementation of the animal welfare in the Philippines.
Other activities of the celebration include: Holy Mass, pet blessing and oath taking of the newly elected officers of DA-CAW.
The Animal Welfare Week is celebrated every first week of October by virtue of the Presidential Proclamation No. 715 series of 2004.

  </h4>
  <br>
  <br>
</div>

 <div class="col-sm-3" style="">
         <img src="../uploads/news/news2.jpg" style="width: 400px; height: 300px;margin-top: 60px">
    </div>

     <div class="col-sm-8" style="margin-left: 100px">
      <br>
    <br>
 
   <center><h3>Da-BAI to hold animal welfare kick-off celebration</h3>By: Mark Francis S. España</center> 
    <br>
    <h4>
 This year marks the 10th World Rabies Day Celebration which carries the theme, “Rabies: Educate. Vaccinate. Eliminate.” The celebration will highlight the awarding of rabies-free zones and Best Local Government Unit (LGU) Implementers of the Rabies Program on September 28, 2016 at the Fertilizer and Pesticides Authority Conference Hall, BAI Compound, Visayas Avenue, Diliman, Quezon City.
 <br><br>
The NRPCC has declared 35 rabies-free zones since 2008. This year, three zones will be added to the list. These are the Municipality of Romblon, Romblon; Municipality of San Jose, Romblon; and Pan de Azukar Islands of Concepcion, Iloilo. This initiative is jointly handled by the DOH and DA-BAI as mandated by the Joint DOH-DA Order No. 1, Series of 2008 (“Guidelines for Declaring Rabies-Free Zones”).
<br><br>
Furthermore, 37 LGUs from all over the country will be awarded as Best Rabies Program Implementers. This is to recognize the LGU's efforts in educating the community, vaccinating the dogs and hopefully, eliminating the deadly disease by 2020. Recognition of the best implementers was made possible through the funding of the World Organisation for Animal Health (OIE)-BAI Stop Transboundary Animal Diseases and Zoonoses (STANDZ) Rabies Project.
Pursuant to the Republic Act No. 9482 or the “Anti-Rabies Act of 2007”, the Department of Agriculture (DA) through BAI is mandated to implement the National Rabies Prevention and Control Program together with the Department of Health-Disease Prevention and Control Bureau (DOH-DPCB), Department of Interior and Local Government (DILG), Department of Education (DepEd), Department of Environment and Natural Resources (DENR), and partners from the academe, Non-Government Organizations (NGOs), and People's Organization.
<br><br>
World Rabies Day is an annual global campaign, initiated by the Global Alliance for Rabies Control (GARC) in partnership with the United States Centers for Disease Control and Prevention (US CDC), World Health Organization (WHO), OIE and the Pan American Health Organization (PAHO). It aims to raise awareness on the impact of human and canine rabies, its prevention and control, and to eliminate the disease for both humans and animals.

  </h4>
  <br>
  <br>
</div>



  <div class="col-sm-3" style="">
        <img src="../uploads/news/news3.jpg" style="width: 400px; height: 300px;margin-top: 60px">
    </div>

      <div class="col-sm-8" style="margin-left: 100px">
      <br>
    <br>
 
   <center><h3>NRPCC to Conduct Rabies Awareness Month Kick-off Activity</h3>Written by: Edrian P. Paras</center> 
    <br>
    <h4>
The National Rabies Prevention and Control Committee (NRPCC) chaired by the Bureau of Animal Industry (BAI) will hold a Rabies Awareness Month Kick-off Activity on March 2, 2016 at the Muntinlupa City Sports Complex.
With this year’s theme “End Rabies, Now Na!," the activity aims to strengthen the national campaign to control and further eliminate the disease in the country. This will also serve as a venue to provide proper knowledge on the prevention and management of Rabies.
BAI Director Rubina O. Cresencio, Department of Health (DOH) Rabies Program Manager Dr. Ernesto ES. Villalon III, City Mayor Jaime R. Fresnedi and Born to be Wild Hosts Dr. Ferdinand Recio and Dr. Neilsen B. Donato will grace the opening ceremony.The celebration will highlights the different activities such as the ceremonial turn-over of school-based materials by the Global Alliance for Rabies Control (GARC) in coordination with Knowledge Channel and Muntinlupa City Early Childhood Education Division; mass vaccination for dogs and cats; puppet show and awarding of hero dogs.


  </h4>
  <br>
  <br>
</div>
  <div class="col-sm-3" style="">
        <img src="../uploads/news/news4.jpg" style="width: 400px; height: 300px;margin-top: 60px">
    </div>

      <div class="col-sm-8" style="margin-left: 100px">
      <br>
    <br>
 
   <center><h3>NRPCC to Conduct Rabies Awareness Month Kick-off Activity</h3>Marilenews</center> 
    <br>
    <h4>
The National Rabies Prevention and Control Committee (NRPCC) chaired by the Bureau of Animal Industry (BAI) will hold a Rabies Awareness Month Kick-off Activity on March 2, 2016 at the Muntinlupa City Sports Complex.
With this year’s theme “End Rabies, Now Na!," the activity aims to strengthen the national campaign to control and further eliminate the disease in the country. This will also serve as a venue to provide proper knowledge on the prevention and management of Rabies.
BAI Director Rubina O. Cresencio, Department of Health (DOH) Rabies Program Manager Dr. Ernesto ES. Villalon III, City Mayor Jaime R. Fresnedi and Born to be Wild Hosts Dr. Ferdinand Recio and Dr. Neilsen B. Donato will grace the opening ceremony.The celebration will highlights the different activities such as the ceremonial turn-over of school-based materials by the Global Alliance for Rabies Control (GARC) in coordination with Knowledge Channel and Muntinlupa City Early Childhood Education Division; mass vaccination for dogs and cats; puppet show and awarding of hero dogs.


  </h4>
  <br>
  <br>
</div>

</div>
</div> -->


<br>
<br>
<br>
<?php include '../template/footer.php'?>