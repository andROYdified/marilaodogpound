<?php
//session_start();
	include '../template/headeruser.php';
	
	require_once 'dbconfig.php';
	
	if(isset($_GET['delete_id']))
	{
		// select image from db to delete
		$stmt_select = $DB_con->prepare('SELECT userPic FROM tbl_users WHERE userID =:uid');
		$stmt_select->execute(array(':uid'=>$_GET['delete_id']));
		$imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
		unlink("user_images/".$imgRow['userPic']);
		
		// it will delete an actual record from db
		$stmt_delete = $DB_con->prepare('DELETE FROM tbl_users WHERE userID =:uid');
		$stmt_delete->bindParam(':uid',$_GET['delete_id']);
		$stmt_delete->execute();
		
		header("Location: index.php");
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
<title>Complain Report</title>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
</head>

<body style="background-color: #FFECB3">

<!--<div class="navbar navbar-default navbar-static-top" role="navigation">
   <!-- <div class="container">
 
        <div class="navbar-header">
            <a class="navbar-brand" href="http://www.codingcage.com" title='Programming Blog'>Coding Cage</a>
            <a class="navbar-brand" href="http://www.codingcage.com/search/label/CRUD">CRUD</a>
            <a class="navbar-brand" href="http://www.codingcage.com/search/label/PDO">PDO</a>
            <a class="navbar-brand" href="http://www.codingcage.com/search/label/jQuery">jQuery</a>
        </div>
 
    </div>
</div>-->

<div class="container">

	<!--<div class="page-header">
    	<h1 class="h2">All Complains. / <a class="btn btn-primary" href="addnew.php"> <span class="glyphicon glyphicon-plus"></span> &nbsp; add new </a></h1> 
    </div>-->
    
<br />
  <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                            <div class="alert alert-info">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong><i class="icon-user icon-large"></i>&nbsp;<center>Complain Report</center></strong>
                            </div>
                          </table>
                          <table class="table table-bordered">
                           <thead>
                               <!-- <tr>
                                    <th style="text-align:center;">DogID</th>
                                    <th style="text-align:center;">Image</th>
                                    <th style="text-align:center;">Name</th>
                               
                                    <th style="text-align:center;">Action</th>
                                </tr>
                            </thead>
                            <tbody> 
                            </table>-->
<div class="row" style="">
<?php
	$id=$_SESSION['userID'];
	//$stmt = $DB_con->prepare('SELECT a.userID, userName, userProfession, userPic FROM tbl_users a, userinfo u  WHERE a.userID=:id AND u.userID=:id');
	$stmt = $DB_con->prepare('SELECT * from complainreport');



		$stmt->bindParam(":id", $id);
	$stmt->execute();
	
	if($stmt->rowCount() > 0)
	{
		while($row=$stmt->fetch(PDO::FETCH_ASSOC))
		{
			extract($row);
			?>
			<div class="col-xs-12">

		<center>
				
				<!--<p class="page-header">
				<?php echo $compInfo."&nbsp;/&nbsp;".$compLocation
				.$compDate; ?></p>-->


				<img src="user_images/<?php echo $row['compImage']; ?>" class="img-rounded" width="150px" height="150px" margin-top="550px" />

				<div style="margin-left: 0px">
	
		<?php echo $compInfo."&nbsp;/&nbsp;".$compLocation 
		
			.$compDate; ?>
			</div>
				<p class="page-header" style="">
				<span>
				
				<a class="btn btn-info" href="editform.php?edit_id=<?php echo $row['userID']; ?>" title="click for edit"  onclick="return confirm('sure to edit ?')"><span class="glyphicon glyphicon-edit"></span> Edit</a> 
				<a class="btn btn-danger" href="?delete_id=<?php echo $row['userID']; ?>" title="click for delete" onclick="return confirm('sure to delete ?')"><span class="glyphicon glyphicon-remove-circle"></span> Delete</a>
				</span>
				</p>
				
	</center>
			</div>      
			<?php
		}
	}
	else
	{
		?>
        <div class="col-xs-12">
        	<div class="alert alert-warning">
            	<span class="glyphicon glyphicon-info-sign"></span> &nbsp; No Data Found ...
            </div>
        </div>
        <?php
	}
	
?>
</div>	



<!--<div class="alert alert-info">
    <strong>tutorial link !</strong> <a href="http://www.codingcage.com/2016/02/upload-insert-update-delete-image-using.html">Coding Cage</a>!
</div>-->

</div>


<!-- Latest compiled and minified JavaScript -->
<script src="bootstrap/js/bootstrap.min.js"></script>


</body>
</html>

<!--<?php include '../template/footer.php'; ?>