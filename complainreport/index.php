<?php include('header.php');
require_once('db.php');
 ?>
<body>


    <div class="row-fluid">
        <div class="span12">


         

            <div class="container">
			
<?php include ('modal_add.php'); ?>

	<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong><i class="icon-user icon-large"></i>&nbsp;Dog Information </strong>
		</div>
		<thead>
			<!-- <tr>
				<th style="text-align:center;">DogID</th>-->
				<th style="text-align:center;">#</th>
				<th style="text-align:center;">Complain Image</th>
					<th style="text-align:center;">Complain Information</th>                                
					<th style="text-align:center;">Complain Location</th>                                
					<th style="text-align:center;">Complain Date</th>                                
					<th style="text-align:center;">User</th>                                
												

				<th style="text-align:center;">Action</th>                                
		</thead>
		<tbody>
		<?php
			
			$result = $conn->prepare("SELECT c.compID, c.compImage, c.compImage, c.compInfo, c.compLocation, c.compDate, c.isNotified,
			u.userFirstname, u.userLastname, u.userContactno FROM complainreport c
			INNER JOIN userinfo u ON c.userID = u.userID ORDER BY compID ASC");
			$result->execute();
			for($i=0; $row = $result->fetch(); $i++)
			{
			$id=$row['compID'];
			$results = $conn->prepare("SELECT complainreport FROM  where compID=".$row['compID']);
			$results->execute();
				for($ii=0; $srow = $results->fetch(); $ii++){
									$dstatus=$srow['compID'];															
				}									

		?>
			<tr>																
			
			<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['compID']; ?></td>
			<td style="text-align:center; margin-top:10px; word-break:break-all; width:450px; line-height:100px;">
			<?php 
			$picture="../uploads/default.png";
			if($row['compImage'] != "" ) $picture='../uploads/'.$row['compImage'];										
			?>
			<img src="<?php echo $picture; ?>" width="100px" height="100px" style="border:1px solid #333333;">
			</td>
			
			<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['compInfo']; ?></td>
			
			<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['compLocation']; ?></td>
			<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['compDate']; ?></td>
			<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['userFirstname'] . ' ' . $row ['userLastname']; ?></td>
			
		
			<td style="text-align:center; width:350px;">
					<a href="#updte_img<?php echo $id;?>"  data-toggle="modal"  class="btn btn-warning" >Update</a>
					<br>									 
					<br>									 
					<a href="#delete<?php echo $id;?>"  data-toggle="modal"  class="btn btn-danger" >Delete </a>
					<br>
					<br>
					<button <?php if($row['isNotified'] == 1) echo "disabled='disabled'"; else echo ""; ?>  class="btn btn-secondary btn-lg btnNotify" id="<?= $row['compID'] ?>" data="<?= $row['userContactno'] ?>">Notify Sender</button>
			</td>
			</tr>
					<!-- Modal -->
		<div id="delete<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
		<h3 id="myModalLabel">Delete</h3>
		</div>
		<div class="modal-body">
		<div class="alert alert-danger">
		<?php 
			$picture="..uploads/default.png";
			if($row['picture'] != "" ) $picture='../uploads/'.$row['picture'];										
			?>
			<img src="<?php echo $picture; ?>" width="100px" height="100px" style="border:1px solid #333333;">	
		<b style="color:blue; margin-left:25px; font-size:30px;"><?php echo $row['compID']." ".$row['compInfo']; ?></b>
		<br />
		<p style="font-size: larger; text-align: center;">Are you Sure you want to Delete?</p>
		</div>
		<hr>
		<div class="modal-footer">
		<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
		<a href="delete.php<?php echo '?id='.$id; ?>" class="btn btn-danger">Yes</a>
		</div>
		</div>
		</div>
	<!-- Modal Update Image -->
		<div id="updte_img<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header"><h3 id="myModalLabel">Update</h3></div>

		<div class="modal-body">
		<div class="alert alert-danger">
		
		<form action="edit.php<?php echo '?id='.$id; ?>" method="post" enctype="multipart/form-data">											
			
			<!--<label style="color:#3a87ad; font-size:18px;" >Upload Picture</label>-->
				
			<?php 
			$picture="..uploads/default.png";
			if($row['picture'] != "" ) $picture='../uploads/'.$row['picture'];										
			?>
			<img src="<?php echo $picture; ?>" width="100px" height="100px" style="border:1px solid #333333;">				
			<div style="color:blue; margin-left:150px; font-size:30px;">
			<input type="file" name="picture" style="margin-top:-115px;" value="<?php echo $row['picture'];?>"></div>

			<label style="color:#3a87ad; font-size:18px;" >Dogs Name</label>								
				<input type="text" name="dogname" value="<?php echo $row['dogname'];?>">							
			<label style="color:#3a87ad; font-size:18px;" >Status</label>								
			<select  name="status" style="">
				<?php 
					$resulte = $conn->prepare("SELECT * FROM complainreport ORDER BY compID ASC");
					$resulte->execute();
					for($ie=0; $erow = $resulte->fetch(); $ie++){
						echo '<option value="'.$id=$erow['compID'].'">'.$id=$erow['compInfo'].'</option>';
				}?>
			</select>	
<!-- 								
			<label style="color:#3a87ad; font-size:18px;" >Lastname</label>
				<input type="text" name="lastname" value="<?php echo $row['lastname'];?>">							
			<label style="color:#3a87ad; font-size:18px;" >Firstname</label>
				<input type="text" name="firstname" value="<?php echo $row['firstname'];?>">							
			<label style="color:#3a87ad; font-size:18px;" >Middlename</label>
				<input type="text" name="middlename" value="<?php echo $row['middlename'];?>">							
			<label style="color:#3a87ad; font-size:18px;" >Contact No.</label>
				<input type="text" name="contactno" value="<?php echo $row['contactno'];?>">															
			<label style="color:#3a87ad; font-size:18px;" >Reason</label>
				<input type="text" name="reason" value="<?php echo $row['reason'];?>">															 -->
																									
		</div>

		<hr>
		<div class="modal-footer">
		<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
		<button type="submit" name="submit" class="btn btn-danger">Yes</button>
		</form>
		</div>
		</div>
		</div>
			<?php } ?>
		</tbody>
	</table>


          
        </div>
        </div>
        </div>
    </div>
	<script
		src="http://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>
	<script>
		$(document).ready(function() {

			$(".btnNotify").click(function(){
				var number = $(this).attr('data');
                var message = "Ano message ilalagay ko dito? hahah";
				var id = $(this).attr('id');

                $.ajax({
                    url : '../sms/sendSms.php',
                    data: { 'message' : message, 'number' : number, 'id': id},
                    method: 'POST',
                    dataType: 'json',
                    success: function(data){
                        alert(data.message);
						$("#" + id).attr("disabled", "disabled");
                    }
                })

			});
		});
	</script>


</body>
</html>


