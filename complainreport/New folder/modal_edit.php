<!-- Modal Update Image -->
<div id="updte_img<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">			
	<div class="modal-header"><h3 id="myModalLabel">Update</h3></div>
		<div class="modal-body">
			<div class="alert alert-danger">
		
			<form action="edit.php<?php echo '?id='.$id; ?>" method="post" enctype="multipart/form-data">		
							
				<?php 
					$picture="images/default.png";
					if($row['picture'] != "" ) $picture='uploads/'.$row['picture'];										
				?>
				<img src="<?php echo $picture; ?>" width="100px" height="100px" style="border:1px solid #333333;">				
				<div style="color:blue; margin-left:150px; font-size:30px;">
					<div>
					<input 	type="file" 
							name="picture" 
							style="margin-top:-115px;" 
							value="<?php echo $row['picture'];?>">
					</div>
					<label>Dog's Name</label>
					<input 	type="text" 
							name="dogname" 
							value="<?php echo $row['dogname'];?>">	
							
					<label>Status</label>		
					<select  name="status" style="">
						<?php 
						$resulte = $conn->prepare("SELECT * FROM dogstatus ORDER BY id ASC");
						$resulte->execute();
						for($ie=0; $erow = $resulte->fetch(); $ie++){
							echo '<option value="'.$id=$erow['id'].'">'.$id=$erow['status'].'</option>';
						}?>
					</select>
												
					<label>Lastname</label>
					<input 	type="text" 
							name="lastname" 
							value="<?php echo $row['lastname'];?>">	
													
					<label>Firstname</label><input type="text" name="firstname" value="<?php echo $row['firstname'];?>">
					<label>Middlename</label><input type="text" name="middlename" value="<?php echo $row['middlename'];?>">				
					<label>Contact No.</label><input type="text" name="contactno" value="<?php echo $row['contactno'];?>">					
					<label>Reason</label><input type="text" name="reason" value="<?php echo $row['reason'];?>">
					
				</div>
			<div class="modal-footer">
				<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
				<button type="submit" name="submit" class="btn btn-danger">Yes</button>			
			</div>
			</form>
		</div>
	</div>
</div>              
