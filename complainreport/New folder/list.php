<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">

		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong><i class="icon-user icon-large"></i>&nbsp;Dog Information</strong>
		</div>

		<thead>
			<!-- <tr>
			<th style="text-align:center;">DogID</th>-->
			<th style="text-align:center;">#</th>
			<th style="text-align:center;">Image</th>
			<th style="text-align:center;">Dog's Name</th>
			<th style="text-align:center;">Status</th>
			<th style="text-align:center;">Lastname</th>
			<th style="text-align:center;">Firstname</th>
			<th style="text-align:center;">Middlename</th>
			<th style="text-align:center;">Contact No.</th>
			<th style="text-align:center;">Reason</th>
			<th style="text-align:center;">Date Registered</th>

		</thead>
		
		<tbody>
		<?php
			require_once('db.php');
			$result = $conn->prepare("SELECT * FROM dogfiles ORDER BY id ASC");
			$result->execute();
			for($i=0; $row = $result->fetch(); $i++){
				$id=$row['id'];		
					$results = $conn->prepare("SELECT status FROM dogstatus where id=".$row['status']);
					$results->execute();
					
					for($ii=0; $srow = $results->fetch(); $ii++){
						$dstatus=$srow['status'];
							
					}
		?>
		<tr>
			<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['id']; ?></td>
			
			<td style="text-align:center; margin-top:10px; word-break:break-all; width:450px; line-height:100px;">
				<?php 
					$picture="../uploads/default.png";
					if($row['picture'] != "" ) $picture='../uploads/'.$row['picture'];										
				?>
				<img src="<?php echo $picture; ?>" width="100px" height="100px" style="border:1px solid #333333;">
			</td>

			<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['dogname']; ?></td>
			<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $dstatus; ?></td>
			<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['lastname']; ?></td>
			<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['firstname']; ?></td>
			<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['middlename']; ?></td>
			<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['contactno']; ?></td>
			<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['reason']; ?></td>
			<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['dateregister']; ?></td>

			<td style="text-align:center; width:350px;">
				<a href="#updte_img<?php echo $id;?>"  data-toggle="modal"  class="btn btn-warning" >Update</a>
				<br>
				<br>
				<a href="#delete<?php echo $id;?>"  data-toggle="modal"  class="btn btn-danger" >Delete </a>
			</td>
		</tr>

		<!-- Modal -->
		<div id="delete<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header"><h3 id="myModalLabel">Delete</h3></div>
			
			<div class="modal-body">
				<div class="alert alert-danger">

					<?php 
						$picture="../uploads/default.png";
						if($row['picture'] != "" ) $picture='../uploads/'.$row['picture'];										
					?>
					<img src="<?php echo $picture; ?>" width="100px" height="100px" style="border:1px solid #333333;">
					<b style="color:blue; margin-left:25px; font-size:30px;"><?php echo $row['id']." ".$row['picture']; ?></b>					
					<p style="font-size: larger; text-align: center;">Are you Sure you want to Delete?</p>
				</div>

				
				<div class="modal-footer">
					<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
					<a href="delete.php<?php echo '?id='.$id; ?>" class="btn btn-danger">Yes</a>
				</div>
			</div>
		</div>

<?php include ('modal_edit.php'); ?>		
<?php } ?>
</tbody>
</table>