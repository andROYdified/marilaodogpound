<?php if($row['doginfoImage'] != ""): ?>
<img src="uploads/<?php echo $row['doginfoImage']; ?>" width="100px" height="100px" style="border:1px solid #333333;">
<?php else: ?>
<img src="images/default.png" width="100px" height="100px" style="border:1px solid #333333;">
<?php endif; ?>
</td>
<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['doginfoGender']; ?></td>
<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['doginfoBreed']; ?></td>
<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['doginfoColor']; ?></td>
<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['doginfoSize']; ?></td>
<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['doginfoStatus']; ?></td>
<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['doginfoDateRegister']; ?></td>
<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['doginfoScore']; ?></td>
<td style="text-align:center; width:350px;">
<a href="#updte_img<?php echo $id;?>"  data-toggle="modal"  class="btn btn-warning" >Update</a>
<br>
<br>
<a href="#delete<?php echo $id;?>"  data-toggle="modal"  class="btn btn-danger" >Delete </a>
</td>
</tr>
<!-- Modal -->
<div id="delete<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header">
<h3 id="myModalLabel">Delete</h3>
</div>
<div class="modal-body">
<div class="alert alert-danger">
<?php if($row['doginfoImage'] != ""): ?>
<img src="uploads/<?php echo $row['doginfoImage']; ?>" width="100px" height="100px" style="border:1px solid #333333;">
<?php else: ?>
<!--<img src="images/default.png" width="100px" height="100px" style="border:1px solid #333333; margin-left:15px;">-->
<img src="images/default.png" width="100px" height="100px" style="border:1px solid #333333; margin-left:15px;" value="<?php echo['doginfoImage'];?>">
<?php endif; ?>
<b style="color:blue; margin-left:25px; font-size:30px;"><?php echo $row['doginfoID']." ".$row['doginfoImage']; ?></b>
<br />
<p style="font-size: larger; text-align: center;">Are you Sure you want to Delete?</p>
</div>
<hr>
<div class="modal-footer">
<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
<a href="delete.php<?php echo '?doginfoID='.$id; ?>" class="btn btn-danger">Yes</a>
</div>
</div>
</div>
<!-- Modal Update Image -->
<div id="updte_img<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header">
<h3 id="myModalLabel">Update</h3>

</div>

<div class="modal-body">
<div class="alert alert-danger">
<?php if($row['doginfoImage'] != ""): ?>

<img src="uploads/<?php echo $row['doginfoImage']; ?>" width="100px" height="100px" style="border:1px solid #333333; margin-left: 30px;" value="<?php echo $row['doginfoImage']; ?>">
<?php else: ?>

<img src="uploads/<?php echo $row['doginfoImage']; ?>" width="100px" height="100px" style="border:1px solid #333333; margin-left: 30px;" value="<?php echo $row['doginfoImage']; ?>">

<?php endif; ?>
<form action="edit_PDO.php<?php echo '?doginfoID='.$id; ?>" method="post" enctype="multipart/form-data">
<div style="color:blue; margin-left:150px; font-size:30px;">
<input type="file" name="doginfoImage" style="margin-top:-115px;" value="<?php echo $row['doginfoImage'];?>">
<label>Gender</label>
<select  name="doginfoGender" style="" value="<?php echo $row['doginfoGender']; ?>">
<option value="MALE">MALE</option>
<option value="FEMALE">FEMALE</option>
</select>

<label>Breed</label>
<select  name="doginfoBreed" style="" value="<?php echo $row['doginfoBreed']; ?>">
<option value="PUREBREED">PUREBREED</option>
<option value="MONGREL">MONGREL</option>
<option value="MIXEDBREED">MIXEDBREED</option>
</select>
<label style="">Color</label>
<select  name="doginfoColor" style="" value="<?php echo $row['doginfoColor']; ?>">
<option value="WHITE">WHITE</option>
<option value="BLACK">BLACK</option>
<option value="BROWN">BROWN</option>
<option value="MIXED">MIXED</option>
</select>
<label>Size</label>
<select  name="doginfoSize" style="" value="<?php echo $row['doginfoSize']; ?>">
<option value="SMALL">SMALL</option>
<option value="MEDIUM">MEDIUM</option>
<option value="LARGE">LARGE</option>

</select>
<label>Status</label>
<select  name="doginfoStatus" style="" value="<?php echo $row['doginfoStatus']; ?>">
<option value="ADOPTABLE">ADOPTABLE</option>
<option value="SURRENDERED">SURRENDERED</option>
<option value="IMPOUNDED">IMPOUNDED</option>


</select>

<label style="color:#3a87ad; font-size:18px;" >Date Register</label>
<input type="date" name="doginfoDateRegister"
value="<?php echo $row['doginfoDateRegister'];?>">
<label>Score</label>
<input type="number" name="doginfoScore" value="<?php echo $row['doginfoScore']; ?>">


</div>
<hr>
<div class="modal-footer">
<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
<button type="submit" name="submit" class="btn btn-danger">Yes</button>
</form>
</div>
</div>
</div>
<?php } ?>
</tbody>
</table>