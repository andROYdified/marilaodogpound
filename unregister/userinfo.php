<?php 
	include '../database/db.php';
	if ($_POST)
	{
			$targetdir	=	"../uploads/userinfo/";
			$targetfile	=	$targetdir.basename($_FILES['userImage']['name']);
			$uploadOk	=	1;
			$imageType	=	pathinfo($targetfile,PATHINFO_EXTENSION);
			move_uploaded_file($_FILES['userImage']['tmp_name'],$targetfile);
			echo "The image is uploaded";
		try{
			$query	="INSERT INTO userinfo SET 
				acctID=?, 
				userImage=?, 
				userFirstname=?,
				userMiddlename=?,
				userLastname=?,
				userBlkno=?,
				userLotno=?,
				userStreet=?,
				userBarangay=?,
				userMunicipality=?,
				userProvince=?,
				userContactno=?,
				userBdate=?,
				userGender=?,
				userStatus=?
				";
			$stmt	=	$conn->prepare($query);
			$stmt	->	bindParam(1,$_POST['acctID']);
			$stmt	->	bindParam(2,$targetfile);
		
			$stmt	->	bindParam(3,$_POST['userFirstname']);
			$stmt	->	bindParam(4,$_POST['userMiddlename']);
			$stmt	->	bindParam(5,$_POST['userLastname']);
			$stmt	->	bindParam(6,$_POST['userBlkno']);
			$stmt	->	bindParam(7,$_POST['userLotno']);
			$stmt	->	bindParam(8,$_POST['userStreet']);
			$stmt	->	bindParam(9,$_POST['userBarangay']);
			$stmt	->	bindParam(10,$_POST['userMunicipality']);
			$stmt	->	bindParam(11,$_POST['userProvince']);
			$stmt	->	bindParam(12,$_POST['userContactno']);
			$stmt	->	bindParam(13,$_POST['userBdate']);
			$stmt	->	bindParam(14,$_POST['userGender']);
			$stmt	->	bindParam(15,$_POST['userStatus']);




			if ($stmt	->	execute())
			{
				header('location:../unregister/successinfo.php');
			}else{
				header('location:../unregister/failedinfo.php');
			}
		}catch (PDOException $e) {
		echo "ERROR ".$e->getMessage();}
	}else
	header('location:../unregister/userinfoform.php');
 ?>