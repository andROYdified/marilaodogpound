<?php include('header.php');
require_once('db.php');
session_start();
$uid=$_SESSION['userID'];

echo $uid;
?>
<body>

<div class="row-fluid">
<div class="span12">

<div class="container">

<!-- Button to trigger modal -->

<a class="btn btn-primary" href="#myModal" data-toggle="modal">Click Here To Add</a>
<br>
<br>
<br>
<a href="../administrator/navigationinline.php"><button class="btn btn-primary">Back</button></a>
<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header"><h3 id="myModalLabel">Add Dogs Record</h3></div>
<div class="modal-body">

<form method="post" action="add.php"  enctype="multipart/form-data">
<table class="table1">
<?php 
$lbl='<tr><td><label style="color:#3a87ad; font-size:18px;">';
$elbl='</label></td><td width="30"></td>';



echo $lbl.'Select your Image'.$elbl.'<td>	
<td><input type="file" name="picture"></td></tr>';

echo $lbl.'Dogs Name'.$elbl.'<td>		
<td><input type="text" name="dogName" placeholder="dog Name"  /></td></tr>';

echo $lbl.'dogMarkings'.$elbl.'<td>			
<td><input type="text" name="dogMarkings" placeholder="dog Markings"  ></td></tr>';

echo $lbl.'dogStatus'.$elbl.'<td>					
<td><select name="dogStatus">
	<option value="NOTVERIFIED">NOT VERIFIED</option>
	<option value="VERIFIED">VERIFIED</option>
	</SELECT>
</td></tr>';

echo $lbl.'dogDateRegister'.$elbl.'<td>
<td><input type="date" name="dogDateRegister"></td></tr>';

echo $lbl.'dogType'.$elbl.'<td></label></td>
<td><input type="text" name="dogType" placeholder="dog Type"  ></td></tr>';

echo $lbl.'dogGender'.$elbl.'<td>					
<td><select name="dogGender">
	<option value="MALE">MALE</option>
	<option value="FEMALE">FEMALE</option>
	</SELECT>
</td></tr>';

echo $lbl.'dogBreed'.$elbl.'<td></label></td>
<td><input type="text" name="dogBreed" placeholder="dog Breed"></td></tr>';

echo $lbl.'dogColor'.$elbl.'<td></label></td>
<td><input type="text" name="dogColor" placeholder="dog Color"></td></tr>';

echo $lbl.'dogSize'.$elbl.'<td></label></td>
<td><input type="text" name="dogSize" placeholder="dog Size"></td></tr>';

echo $lbl.'dogAge'.$elbl.'<td></label></td>
<td><input type="text" name="dogAge" placeholder="dog Age"  ></td></tr>';

echo '<input type="hidden" name="userID" value="'.$uid.'" >';

?>	



</table>
</div>
<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
<button type="submit" name="Submit" class="btn btn-primary">Upload</button>
</div>
</form>
</div>	

<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
<div class="alert alert-info">
<button type="button" class="close" data-dismiss="alert">&times;</button>
<strong><i class="icon-user icon-large"></i>&nbsp;Dog Information </strong>
</div>
<thead>
<!-- <tr>
<th style="text-align:center;">DogID</th>-->
<th style="text-align:center;">#</th>
<th style="text-align:center;">Image</th>
<th style="text-align:center;">Dog's Name</th>                                
<th style="text-align:center;">Markings</th>                                
<th style="text-align:center;">Status</th>                                
<th style="text-align:center;">Date Registered</th>                                
<th style="text-align:center;">Dog Type</th>                                
<th style="text-align:center;">Gender</th>                                
<th style="text-align:center;">Breed</th>                                
<th style="text-align:center;">Size</th>                                
<th style="text-align:center;">Age</th>                                
<th style="text-align:center;">Action</th>                                
</thead>
<tbody>
<?php

$result = $conn->prepare("SELECT * FROM dogregistration where userID=".$uid." order BY dogID ASC");
$result->execute();
for($i=0; $row = $result->fetch(); $i++)
{
$id=$row['dogID'];

?>
<tr>																

<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['dogID']; ?></td>
<td style="text-align:center; margin-top:10px; word-break:break-all; width:450px; line-height:100px;">
<?php 
$picture="../uploads/default.png";
if($row['picture'] != "" ) $picture='../uploads/'.$row['picture'];										
?>
<img src="<?php echo $picture; ?>" width="100px" height="100px" style="border:1px solid #333333;">
</td>

<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['dogName']; ?></td>

<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['dogMarkings']; ?></td>
<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['dogStatus']; ?></td>
<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['dogDateRegister']; ?></td>
<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['dogType']; ?></td>
<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['dogGender']; ?></td>
<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['dogBreed']; ?></td>
<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['dogColor']; ?></td>
<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['dogSize']; ?></td>
<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['dogAge']; ?></td>
<td style="text-align:center; width:350px;">
<a href="#updte_img<?php echo $id;?>"  data-toggle="modal"  class="btn btn-warning" >Update</a>
<br>									 
<br>									 
<a href="#delete<?php echo $id;?>"  data-toggle="modal"  class="btn btn-danger" >Delete </a>
</td>
</tr>
<!-- Modal -->
<div id="delete<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header">
<h3 id="myModalLabel">Delete</h3>
</div>
<div class="modal-body">
<div class="alert alert-danger">
<?php 
$picture="..uploads/default.png";
if($row['picture'] != "" ) $picture='../uploads/'.$row['picture'];										
?>
<img src="<?php echo $picture; ?>" width="100px" height="100px" style="border:1px solid #333333;">	
<b style="color:blue; margin-left:25px; font-size:30px;"><?php echo $row['dogID']." ".$row['dogName']; ?></b>
<br />
<p style="font-size: larger; text-align: center;">Are you Sure you want to Delete?</p>
</div>
<hr>
<div class="modal-footer">
<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
<a href="delete.php<?php echo '?id='.$id; ?>" class="btn btn-danger">Yes</a>
</div>
</div>
</div>
<!-- Modal Update Image -->
<div id="updte_img<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header"><h3 id="myModalLabel">Update</h3></div>

<div class="modal-body">
<div class="alert alert-danger">

<form action="edit.php<?php echo '?id='.$id; ?>" method="post" enctype="multipart/form-data">											

<?php 


$lbl='<label width="30" style="color:#3a87ad; font-size:18px;">';
$elbl='</label>';



echo $lbl.'Select your Image'.$elbl.'	
<input type="file" name="picture" value"'.$row['picture'].'">';

echo $lbl.'Dogs Name'.$elbl.'		
<input type="text" name="dogName" value="'.$row['dogName'].'" placeholder="dog Name"  />';

echo $lbl.'dogMarkings'.$elbl.'			
<input type="text" name="dogMarkings" value="'.$row['dogMarkings'].'"  placeholder="dog Markings"  >';

echo $lbl.'dogStatus'.$elbl.'					
<select name="dogStatus" value="'.$row['dogStatus'].'">
	<option value="NOTVERIFIED">NOT VERIFIED</option>
	<option value="VERIFIED">VERIFIED</option>
	</SELECT>
';

echo $lbl.'dogDateRegister'.$elbl.'
<input type="date" name="dogDateRegister" value="'.$row['dogDateRegister'].'">';

echo $lbl.'dogType'.$elbl.'</label>
<input type="text" name="dogType" placeholder="dog Type"  value="'.$row['dogType'].'">';

echo $lbl.'dogGender'.$elbl.'					
<select name="dogGender">
	<option value="MALE">MALE</option>
	<option value="FEMALE">FEMALE</option>
	</SELECT>
';

echo $lbl.'dogBreed'.$elbl.'</label>
<input type="text" name="dogBreed" placeholder="dog Breed" value="'.$row['dogBreed'].'">';

echo $lbl.'dogColor'.$elbl.'</label>
<input type="text" name="dogColor" placeholder="dog Color" value="'.$row['dogColor'].'">';

echo $lbl.'dogSize'.$elbl.'</label>
<input type="text" name="dogSize" placeholder="dog Size" value="'.$row['dogSize'].'">';

echo $lbl.'dogAge'.$elbl.'</label>
<input type="text" name="dogAge" placeholder="dog Age"  value="'.$row['dogAge'].'">';

echo '<input type="hidden" name="userID"   value="'.$row['userID'].'">';

?>														
										
</div>

<hr>
<div class="modal-footer">
<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
<button type="submit" name="submit" class="btn btn-danger">Yes</button>
</form>
</div>
</div>
</div>
<?php } ?>
</tbody>
</table>



</div>
</div>
</div>
</div>


</body>
</html>


