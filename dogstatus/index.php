<?php include('header.php');
require_once('db.php');
$title='Veterinarian';
?>
<body>
<div class="row-fluid">
	<div class="span12">
		<div class="container">

			<a class="btn btn-primary" href="#myModal" data-toggle="modal">Click Here To Add</a>
			<br>
			<br>
			<a href="../administrator/navigationinline.php"><button class="btn btn-primary">Back</button></a>
			<br>
			<br>
			<button onClick="myFunction()" class="btn btn-success">Print</button>

			<script>
			function myFunction() {
			window.print();
			}
			</script>
			<!-- Modal ADD -->
			<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-header"><h3 id="myModalLabel">Add</h3></div>
				<div class="modal-body">
				<form method="post" action="add.php"  enctype="multipart/form-data">
					<table class="table1">
					<tr><td><label style="color:#3a87ad; font-size:18px;" >Status</label></td><td width="30"></td><td><input type="text" name="status" value=""></td></tr>
					<tr><td><label style="color:#3a87ad; font-size:18px;">Remarks</label></td><td width="30"></td><td><input type="text" name="remarks" value=""></td></tr>
					</table>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
					<button type="submit" name="Submit" class="btn btn-primary">Upload</button>
				</div>
				</form>
			</div>			

			<!-- Modal LIST -->

			<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
				<div class="alert alert-info">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong><i class="icon-user icon-large"></i>&nbsp;<?=$title?></strong>
				</div>
				<thead>
					<th style="text-align:center;">#</th>
					<th style="text-align:center;">Status</th>
					<th style="text-align:center;">Remarks</th>                                
					<th style="text-align:center;">Action</th>                                
				</thead>
				<tbody>
				<?php
					$result = $conn->prepare("SELECT * FROM dogstatus ORDER BY id ASC");
					$result->execute();
					for($i=0; $row = $result->fetch(); $i++){
					$id=$row['id'];
				?>
				<tr>

					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['id']; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['status']; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['remarks']; ?></td>
					<td style="text-align:center; width:350px;">
						<a href="#updte_img<?php echo $id;?>"  data-toggle="modal"  class="btn btn-warning" >Update</a>									 
						<a href="#delete<?php echo $id;?>"  data-toggle="modal"  class="btn btn-danger" >Delete </a></td>
				</tr>
			<!-- Modal DELETE -->
				<div id="delete<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-header"><h3 id="myModalLabel">Delete</h3></div>
						<div class="modal-body">
							<div class="alert alert-danger">
								<b style="color:blue; margin-left:25px; font-size:30px;"><?php echo $row['status']." ".$row['remarks']; ?></b>
								<br /><p style="font-size: larger; text-align: center;">Are you Sure you want to Delete?</p>
							</div>
						<hr>
						<div class="modal-footer">
							<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
							<a href="delete.php<?php echo '?id='.$id; ?>" class="btn btn-danger">Yes</a>
						</div>
					</div>
				</div>
				
			<!-- Modal Update RECORD -->
			<div id="updte_img<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-header"><h3 id="myModalLabel">Update</h3></div>

				<div class="modal-body">
						<div class="alert alert-danger">

							<form action="edit.php<?php echo '?id='.$id; ?>" method="post" enctype="multipart/form-data">

							<label style="color:#3a87ad; font-size:18px;" >Status</label><input type="text" name="status" value="<?=$row['status']?>">
							<label style="color:#3a87ad; font-size:18px;" >Remarks</label><input type="text" name="remarks" value="<?=$row['remarks']?>">

						</div>
						<hr>
					
						<div class="modal-footer">
							<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
							<button type="submit" name="submit" class="btn btn-danger">Yes</button>
							</form>
						</div>
					</div>
			</div>
<?php } ?>
			</tbody>
			</table>

</div>
</div>


</body>
</html>


