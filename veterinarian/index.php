<?php include('header.php');
require_once('db.php');
session_start();
$uid=$_SESSION['userID'];
$uname=$_SESSION['uname'];

echo 'Welcome '.$uname;

$title='Veterinarian';
?>
<body>
<div class="row-fluid">
	<div class="span12">
		<div class="container">

			<a class="btn btn-primary" href="#myModal" data-toggle="modal">Click Here To Add</a>
			<br>
			<br>
			<a href="../administrator/navigationinline.php"><button class="btn btn-primary">Back</button></a>
			<br>
			<br>
			<button onClick="myFunction()" class="btn btn-success">Print</button>

			<script>
			function myFunction() {
			window.print();
			}
			</script>
			<!-- Modal ADD -->
			<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-header"><h3 id="myModalLabel">Add</h3></div>
				<div class="modal-body">
				<form method="post" action="add.php"  enctype="multipart/form-data">
					<table class="table1">
					
					<tr><td><label style="color:#3a87ad; font-size:18px;" >Veterinarian</label></td><td width="30"></td>
					<td><select  name="userID" style="">
					<?php
						$sql='SELECT userFirstname,userMiddlename,userLastname,userID, userType
								FROM Account LEFT JOIN Userinfo ON Account.acctID = Userinfo.acctID
								WHERE Account.userType="VETERINARIAN"';
		
						$results = $conn->prepare($sql);
						$results->execute();					
						for($ii=0; $srow = $results->fetch(); $ii++){
							$userid=$srow['userID'];
							$unm=$srow['userFirstname'].' '.$srow['userMiddlename'].'. '.$srow['userLastname'];
							echo '<option value="'.$userid.'">'.$unm.'</option>';
						}
					?>
						</select></td></tr>
					
					
					<tr><td><label style="color:#3a87ad; font-size:18px;">vetPermitno</label></td><td width="30"></td><td>
					<input type="text" name="vetPermitno" value=""></td></tr>
					
					<tr><td><label style="color:#3a87ad; font-size:18px;">vetClinic</label></td><td width="30"></td><td>
					<input type="text" name="vetClinic" value=""></td></tr>
					
					<tr><td><label style="color:#3a87ad; font-size:18px;">vetClinicno</label></td><td width="30"></td><td>
					<input type="text" name="vetClinicno" value=""></td></tr>
					
					<tr><td><label style="color:#3a87ad; font-size:18px;">vetContactno</label></td><td width="30"></td><td>
					<input type="text" name="vetContactno" value=""></td></tr>
					
					<tr><td><label style="color:#3a87ad; font-size:18px;">vetBlkno</label></td><td width="30"></td><td>
					<input type="text" name="vetBlkno" value=""></td></tr>
					
					<tr><td><label style="color:#3a87ad; font-size:18px;">vetLotno</label></td><td width="30"></td><td>
					<input type="text" name="vetLotno" value=""></td></tr>
					
					<tr><td><label style="color:#3a87ad; font-size:18px;">vetStreet</label></td><td width="30"></td><td>
					<input type="text" name="vetStreet" value=""></td></tr>
					
					<tr><td><label style="color:#3a87ad; font-size:18px;">vetBarangay</label></td><td width="30"></td><td>
					<input type="text" name="vetBarangay" value=""></td></tr>
					
					<tr><td><label style="color:#3a87ad; font-size:18px;">vetMunicipality</label></td><td width="30"></td><td>
					<input type="text" name="vetMunicipality" value=""></td></tr>
					
					<tr><td><label style="color:#3a87ad; font-size:18px;">vetProvince</label></td><td width="30"></td><td>
					<input type="text" name="vetProvince" value=""></td></tr>													
					
					</table>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
					<button type="submit" name="Submit" class="btn btn-primary">Upload</button>
				</div>
				</form>
			</div>			

			<!-- Modal LIST -->

			<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
				<div class="alert alert-info">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong><i class="icon-user icon-large"></i>&nbsp;<?=$title?></strong>
				</div>
				<thead>
					<th style="text-align:center;">#</th>
					<th style="text-align:center;">Veterinarian</th>
					<th style="text-align:center;">Permit no</th>                                					
					<th style="text-align:center;">Clinic</th>                                
					<th style="text-align:center;">Clinic no</th>                                
					<th style="text-align:center;">Contact no</th>                                
					<th style="text-align:center;">Blkno</th>                                
					<th style="text-align:center;">Lotno</th>                                
					<th style="text-align:center;">Street</th>                                
					<th style="text-align:center;">Barangay</th>                                
					<th style="text-align:center;">Municipality</th>                                
					<th style="text-align:center;">Province</th>  					
					<th style="text-align:center;">Action</th>                                
				</thead>
				<tbody>
				<?php
					$result = $conn->prepare("SELECT * FROM veterinariandetails ORDER BY id ASC");
					$result->execute();
					for($i=0; $row = $result->fetch(); $i++){
					$id=$row['id'];
				?>
				<tr>

					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['id']; ?></td>
					<?php
						$sql='SELECT userFirstname,userMiddlename,userLastname,userID, userType
								FROM Account LEFT JOIN Userinfo ON Account.acctID = Userinfo.acctID
								WHERE Userinfo.userID='.$row ["userID"];
		
						$results = $conn->prepare($sql);
						$results->execute();					
						for($ii=0; $srow = $results->fetch(); $ii++){
							$userType=$srow['userID'];
							$unm=$srow['userFirstname'].' '.$srow['userMiddlename'].'. '.$srow['userLastname'];
						}
					?>					
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $unm; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['vetPermitno']; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['vetClinic']; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['vetClinicno']; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['vetContactno']; ?></td>					
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['vetBlkno']; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['vetLotno']; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['vetStreet']; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['vetBarangay']; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['vetMunicipality']; ?></td>
					<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['vetProvince']; ?></td>
					<td style="text-align:center; width:350px;">
						<a href="#updte_img<?php echo $id;?>"  data-toggle="modal"  class="btn btn-warning" >Update</a>									 
						<a href="#delete<?php echo $id;?>"  data-toggle="modal"  class="btn btn-danger" >Delete </a></td>
				</tr>
			<!-- Modal DELETE -->
				<div id="delete<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-header"><h3 id="myModalLabel">Delete</h3></div>
						<div class="modal-body">
							<div class="alert alert-danger">
								<b style="color:blue; margin-left:25px; font-size:30px;"><?php echo $unm; ?></b>
								<br /><p style="font-size: larger; text-align: center;">Are you Sure you want to Delete?</p>
							</div>
						<hr>
						<div class="modal-footer">
							<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
							<a href="delete.php<?php echo '?id='.$id; ?>" class="btn btn-danger">Yes</a>
						</div>
					</div>
				</div>
				
			<!-- Modal Update RECORD -->
			<div id="updte_img<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-header"><h3 id="myModalLabel">Update</h3></div>

				<div class="modal-body">
						<div class="alert alert-danger">
							<form action="edit.php<?php echo '?id='.$id; ?>" method="post" enctype="multipart/form-data">
								<label style="color:#3a87ad; font-size:18px;" >Veterinarian</label>
									<select  name="userID" value="<?=$row['userID']?>">
									<?php
										$sql='SELECT userFirstname,userMiddlename,userLastname,userID, userType
												FROM Account LEFT JOIN Userinfo ON Account.acctID = Userinfo.acctID
												WHERE Account.userType="VETERINARIAN"';
						
										$results = $conn->prepare($sql);
										$results->execute();					
										for($ii=0; $srow = $results->fetch(); $ii++){
											$userid=$srow['userID'];
											$unm=$srow['userFirstname'].' '.$srow['userMiddlename'].'. '.$srow['userLastname'];
											$vselect='';
											if ($row['userID']==$srow['userID']) {$vselect=' selected="selected"';}
											echo '<option value="'.$userid.'"'.$vselect.'>'.$unm.'</option>';
										}
									?>
						</select>
									
					<label style="color:#3a87ad; font-size:18px;">vetPermitno</label>
					<input type="text" name="vetPermitno" value="<?=$row['vetPermitno']?>">
					
					<label style="color:#3a87ad; font-size:18px;">vetClinic</label>
					<input type="text" name="vetClinic" value="<?=$row['vetClinic']?>">
					
					<label style="color:#3a87ad; font-size:18px;">vetClinicno</label>
					<input type="text" name="vetClinicno" value="<?=$row['vetClinicno']?>">
					
					<label style="color:#3a87ad; font-size:18px;">vetContactno</label>
					<input type="text" name="vetContactno" value="<?=$row['vetContactno']?>">
					
					<label style="color:#3a87ad; font-size:18px;">vetBlkno</label>
					<input type="text" name="vetBlkno" value="<?=$row['vetBlkno']?>">
					
					<label style="color:#3a87ad; font-size:18px;">vetLotno</label>
					<input type="text" name="vetLotno" value="<?=$row['vetLotno']?>">
					
					<label style="color:#3a87ad; font-size:18px;">vetStreet</label>
					<input type="text" name="vetStreet" value="<?=$row['vetStreet']?>">
					
					<label style="color:#3a87ad; font-size:18px;">vetBarangay</label>
					<input type="text" name="vetBarangay" value="<?=$row['vetBarangay']?>">
					
					<label style="color:#3a87ad; font-size:18px;">vetMunicipality</label>
					<input type="text" name="vetMunicipality" value="<?=$row['vetMunicipality']?>">
					
					<label style="color:#3a87ad; font-size:18px;">vetProvince</label>
					<input type="text" name="vetProvince" value="<?=$row['vetProvince']?>">	
					
						</div>
						<hr>					
						<div class="modal-footer">
							<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
							<button type="submit" name="submit" class="btn btn-danger">Yes</button>
							</form>
						</div>
					</div>
			</div>
<?php } ?>
			</tbody>
			</table>

</div>
</div>


</body>
</html>


