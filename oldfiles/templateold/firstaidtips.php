<?php


?>
<!DOCTYPE html>
<html>
<head>
	<title>First Aid Tips</title>
</head>
<body>

First aid for animal bites
MIND YOUR BODY By Willie T. Ong, MD  | Updated May 22, 2010 - 12:00am
http://www.philstar.com/pet-life/576939/first-aid-animal-bites

Having a pet dog or cat can give us happiness and companionship. But there are some risks involved when one deals with animals, especially stray and wild animals.
According to statistics, dogs are more likely to bite people than cats. However, cat bites are more prone to cause infection. Bites from non-immunized pets and wild animals may carry a risk for rabies.
Because of this, we should be aware of certain basic principles in handling an animal bite.
1. Keep the victim calm. By assuring the person and making him comfortable, we can avoid any sudden surges in the blood pressure (from fear) and increase in heart rate. Also, a normal blood pressure and heart rate can slow down bleeding and the spread of infection. 
2. Check the wound area. Wash your hands thoroughly with soap and water before touching the wound. Is it just a small scratch or a deep wound? For deep wounds, you can wear a disposable glove if available.
3. Treatment for minor wounds. If the bite does not break the skin (there’s no blood involved), then you can just wash the area with soap and water. You can place a dab of Povidone iodine on the scratch and cover it with a clean bandage. Usually, no further treatment is necessary.
Lifestyle Feature ( Article MRec ), pagematch: 1, sectionmatch:
4. Treatment for deep wounds. If the bite punctures the outer layer of the skin, especially if there is some bleeding involved, then you should be extra careful in handling the wound. Apply firm pressure on the bleeding area for around 10 minutes until the bleeding stops. Bring the victim immediately to the Emergency Room or the hospital’s Animal Bite Center. The doctor will clean the wound and apply antibiotic cream on the area.
5. Consult a doctor for deep wounds, swelling, redness and oozing from the wound.
6. Check the animal for rabies. Find out more about the animal that bit the victim. If it’s someone’s pet, then ask the owner if the pet has been immunized against rabies. If it’s a stray dog, you can let someone capture it and observe the animal for ten days in a cage.
7. For most animal bite cases, it is still best to seek medical help. Your doctor will focus on the three dangers of animal bites: (1) infection, (2) rabies, and (3) tetanus.
When To Give Rabies Shots
As mentioned, it is best to capture the animal and observe it for 10 days. If the animal does not turn rabid in 10 days, then it doesn’t have rabies and you don’t need anti-rabies shots.
However, if the animal has escaped, or especially for wild animals, we can assume that the animal has rabies and treat accordingly. Seek medical help.
When To Give Tetanus Shots
Aside from rabies, the tetanus bacteria can penetrate the body from any open wound. The recommendation for getting a tetanus shot is every 10 years. However, if your last tetanus shot was more than five years ago or if the wound is deep and dirty, your doctor may still give you an extra tetanus booster shot within 48 hours of the bite. For those with no previous tetanus injections or who can’t remember their immunization status, doctors may give two tetanus injections, a vaccine and an antibody.
Rabies: A Serious Disease
Rabies is a deadly disease caused by the rabies virus. A person gets rabies after being bitten by an infected animal. The usual culprits are stray dogs, cats, bats, raccoons, skunks and foxes. Animals that rarely transmit rabies are rats, rabbits, and squirrels.
Once a person develops the symptoms of rabies, the disease will be very hard to treat. Most cases of rabies are fatal. Symptoms include fever, headache, anxiety, confusion, difficulty swallowing and paralysis.
Vaccination Is The Treatment
Fortunately, there are vaccines available that effectively prevent rabies after an animal bite. Hence, if you see your doctor immediately after getting bitten, then your doctor can give you a series of rabies shots that will prevent you from getting rabies.
Your doctor needs the following information: (1) What animal bit you? (2) Was it a pet or a stray animal? (3) If it was someone’s pet, was the pet vaccinated against rabies? (4) Was the animal acting unusually, which means it could be rabid, or did you provoke the animal? (5) Can we observe the animal for 10 days to determine if it will turn rabid?
In addition, vaccination is especially needed in cases of bites near the head and neck areas. These are more sensitive because the rabies virus can reach and infect the victim’s brain in a shorter span of time.
Rabies shots usually consist of six injections given over a period of 28 days. The first injection is best given on the day of the bite itself.
Here are tips for first aid of Rabies:         
•	Stop the bleeding by applying pressure for several minutes.
•	Wash the wound immediately with soap and running water.
•	Consult immediately even while observing the dog.
•	Observe the dog for 14 days and consult your physician if any of the following occurs:
− Dog becomes wild and runs aimlessly.
− Dog drools (saliva).
− Dog bites any moving or non-moving object.
− Dog does not eat or drink.
− Dog dies within observation period.
•	If the dog cannot be observed (stray dog), or if suspected to be rabid, consult your physician immediately or go to the nearest Animal Bite Treatment Center in your area.
To know its early warning signs, click here.
 
Reference:
Department of Health. (2015, March 5). Rabies Health Advisory. Health Promo – Department of Health (Official Blog). Retrieved from http://www.healthpromo.doh.gov.ph/rabies-health-advisory/.
 
https://mydoctorfinder.com/healthy-blogs/5-basic-first-aid-tips-for-rabies
A dog bit me! What do I do?
Dog bites  may range from minor scratches to severe punctures or lacerations. In any case, the first aid given to the person bitten will be the same:
1.	Make sure that everyone is safe and secure. If the bite was from a dog that attacked you, instruct the owner or person responsible to secure the dog. If this is not possible, leave the area and head to a safe location that is inaccessible to the dog.
2.	Control bleeding by applying direct pressure on the wound using strips of clean cloth or gauze. In some cases a tourniquet may be necessary.
3.	Once the bleeding has been controlled, wash the wound with soap and water. Scrub the wound and surrounding skin for no less than 5 minutes.
4.	Apply a generous amount of povidone iodine (Betadine) or 70% alcohol on the wound.
5.	If antibiotic ointment is available, apply some on the wound and surrounding area. (Examples of antibiotic ointment: Mupirocin, Neosporin, etc.)
6.	Cover the wound with fresh, clean dressing. Use tape to hold the dressing in place.
7.	Regardless of whether or not the bite is minor or severe, contact an Animal Bite Center and seek immediate medical advice.
The same steps should be followed when dealing with cat bites or scratches.
________________________________________
What should I do about the dog?
The dog that bit you should be placed under observation for a period of 10 days. Don’t forget to provide the dog with food and water dog during this time – you don’t want the dog to die of starvation or thirst. Check the dog’s vaccination records and be prepared with this information when you consult a veterinarian and a medical doctor.
If you notice any signs of illness contact your veterinarian and inform your medical doctor of your observations and the veterinarian’s assessment of your dog. If the dog dies, within the 10 day observation period, it is best to have the remains examined to determine the cause of death. Contact an Animal Bite Center or the Bureau of Animal Industries for instructions on how to prepare the remains for examination.
If you are unable to locate or identify the dog that bit you, it is extremely important to seek medical attention and inform your doctor of the situation.
In dog bite situations, medical doctors will often recommend you get post-exposure treatment for rabies regardless of the severity of the bite, even before the observation period of the dog has passed, and whether or not the dog that bit the human is confirmed to be infected with rabies. Please follow the advice of your doctor in this regard.
https://www.petcentrics.ph/first-aid-for-dog-bites-and-scratches/
On Bites: HELP! I was bitten by a dog/cat. Will I get rabies? What do I do?
http://pawsphilippines.weebly.com/dog-bites-help-i-was-bitten-by-a-dog-what-do-i-do.html

If you are bitten by a dog/cat unknown to you (animal is a stray or you do not know if animal has an owner who vaccinates against rabies regularly), call the Animal Bite Center Hotline at telephone # 816-1111.

1. Clean the wound.

2. Consult your doctor or go to your nearest hospital's emergency room for medical help. PGH, San Lazaro Hospital and RITM (Research Institute for Tropical Medicine) are bite centers and offer treatment at a lower cost compared to private hospitals.

3. The animal has to be observed for signs of rabies for 10-14 days (DO NOT KILL THE ANIMAL). Please work with a vet and/or your LGU, barangay or city vet regarding observation.

If you are bitten or nipped by your pet and you have not been properly vaccinating your pet, please follow the steps mentioned above.

IMPORTANT NUMBERS:

Animal Bite Center Hotline
contact #: 816-1111

Dept. of Health's National Center for Disease Prevention and Control (DOH-NCDPC)
contact #: 751-78-00 or 651-78-00 local 2352
________________________________________
IMPORTANT: Dogs and cats are not born with rabies. 
Dogs and cats do not bite for no reason unless they are provoked (i.e., if the human was not able to read or understand the warning signs) or unless they are rabid. Dogs and cats get rabies when they are exposed to other rabies-infected animals.
How to decrease the chances of being infected with rabies?
FOR PET OWNERS:

1. Make sure your pets are vaccinated by a licensed veterinarian every year against rabies. This is required by law every year due to the incidence of rabies in the country. Please work with your trusted vet regarding the vaccination schedule of your pets. Puppies can be vaccinated when they are at least 3-months old.

2. Spay/neuter your pets. 
Spaying and neutering will help reduce the number of the abandoned and unwanted pets in your community. The consequences of stray and homeless animals' overpopulation is well-known. We all know what happens when homeless animals or strays visit our trash cans, defecate on our properties and worse, bite humans. Spaying/neutering also helps decrease hormone-related aggression.

3. Make sure your pets are not allowed to roam loose out in the streets and public places. 


RA9482: The Anti Rabies Act
SEC. 5. Responsibilities of Pet Owners. – All Pet Owners shall be required to:

(a) Have their Dog regularly vaccinated against Rabies and maintain a registration card which shall contain all vaccinations conducted on their Dog, for accurate record purposes. 

(b) Submit their Dogs for mandatory registration. 

(c) Maintain control over their Dog and not allow it to roam the streets or any Public Place without a leash. 

(d) Be a responsible Owner by providing their Dog with proper grooming, adequate food and clean shelter. 

(e) Within twenty-four (24) hours, report immediately any Dog biting incident to the Concerned Officials for investigation or for any appropriate action and place such Dog under observation by a government or private veterinarian. 

(f) Assist the Dog bite victim immediately and shoulder the medical expenses incurred and other incidental expenses relative to the victim's injuries

</body>
</html>