<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">

		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong><i class="icon-user icon-large"></i>&nbsp;Dog Information</strong>
		</div>
		<thead>
			<?php 
				$ahead=array('ID','Status',"Remarks");
				$xlbl='<th style="text-align:center">';
				foreach ($ahead as $vhead) {echo $xlbl.$vhead.'</th>';}
			?>
			
		</thead>		
		<tbody>
		<?php
			require_once('../database/db.php');
			$result = $conn->prepare("SELECT * FROM dogstatus ORDER BY id ASC");
			$result->execute();
			for($i=0; $row = $result->fetch(); $i++){
				$id=$row['id'];														
					
		?>
		<tr>
			

			<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['id']; ?></td>
			
			<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['status']; ?></td>
			<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['remarks']; ?></td>

			<td style="text-align:center; width:350px;">
				<a href="#updte_img<?php echo $id;?>"  data-toggle="modal"  class="btn btn-warning" >Update</a>
				<br>
				<br>
				<a href="#delete<?php echo $id;?>"  data-toggle="modal"  class="btn btn-danger" >Delete </a>
			</td>
		</tr>

		<!-- Modal -->
		<div id="delete<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header"><h3 id="myModalLabel">Delete</h3></div>
			
			<div class="modal-body">
				<div class="alert alert-danger">
					<b style="color:blue; margin-left:25px; font-size:30px;"><?php echo $row['id']." ".$row['status']; ?></b>					
					<p style="font-size: larger; text-align: center;">Are you Sure you want to Delete?</p>
				</div>

				
				<div class="modal-footer">
					<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
					<a href="delete.php<?php echo '?id='.$id; ?>" class="btn btn-danger">Yes</a>
				</div>
			</div>
		</div>
<?php include ('modal_edit.php'); ?>		
<?php } ?>
</tbody>
</table>
