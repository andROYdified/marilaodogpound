<?php include('header.php');
require_once('db.php');
 ?>
<body>


    <div class="row-fluid">
        <div class="span12">


         

            <div class="container">
			
<?php include ('modal_add.php'); ?>

                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                            <div class="alert alert-info">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong><i class="icon-user icon-large"></i>&nbsp;Dog Information Status</strong>
                            </div>
                            <thead>
                               <!-- <tr>
                                    <th style="text-align:center;">DogID</th>-->
                                    <th style="text-align:center;">#</th>
                                    <th style="text-align:center;">Status</th>
                                     <th style="text-align:center;">Remarks</th>                                
                                     <th style="text-align:center;">Action</th>                                
                            </thead>
                            <tbody>
							<?php
								
								$result = $conn->prepare("SELECT * FROM dogstatus ORDER BY id ASC");
								$result->execute();
								for($i=0; $row = $result->fetch(); $i++){
								$id=$row['id'];

								

							?>
								<tr>
																
								
								<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['id']; ?></td>
								<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['status']; ?></td>
								<td style="text-align:center; word-break:break-all; width:200px;"> <?php echo $row ['remarks']; ?></td>
								<td style="text-align:center; width:350px;">
									 <a href="#updte_img<?php echo $id;?>"  data-toggle="modal"  class="btn btn-warning" >Update</a>									 
									 <a href="#delete<?php echo $id;?>"  data-toggle="modal"  class="btn btn-danger" >Delete </a>
								</td>
								</tr>
										<!-- Modal -->
							<div id="delete<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-header">
							<h3 id="myModalLabel">Delete</h3>
							</div>
							<div class="modal-body">
							<div class="alert alert-danger">
							
							<b style="color:blue; margin-left:25px; font-size:30px;"><?php echo $row['status']." ".$row['remarks']; ?></b>
							<br />
							<p style="font-size: larger; text-align: center;">Are you Sure you want to Delete?</p>
							</div>
							<hr>
							<div class="modal-footer">
							<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
							<a href="delete.php<?php echo '?id='.$id; ?>" class="btn btn-danger">Yes</a>
							</div>
							</div>
							</div>
										<!-- Modal Update Image -->
							<div id="updte_img<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-header">
							<h3 id="myModalLabel">Update</h3>

							</div>

							<div class="modal-body">
							<div class="alert alert-danger">
							
							<form action="edit.php<?php echo '?id='.$id; ?>" method="post" enctype="multipart/form-data">
						
						

		<label style="color:#3a87ad; font-size:18px;" >Status</label>
		<input type="text" name="status"
		value="<?php echo $row['status'];?>">
		
		<label>Remarks</label>
		<input type="text" name="remarks" value="<?php echo $row['remarks']; ?>">

								
		</div>
							<hr>
							<div class="modal-footer">
							<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
							<button type="submit" name="submit" class="btn btn-danger">Yes</button>
							</form>
							</div>
							</div>
							</div>
								<?php } ?>
                            </tbody>
                        </table>


          
        </div>
        </div>
        </div>
    </div>


</body>
</html>


