<?php 

include '../template/header.php';


?>

<!DOCTYPE html>
<html>
<head>

<title>Personal Information</title>



</head>

<body style="background-color: FAFAD2">
	
<div class="form">
	<h2>Personal Information</h2>
	<hr>
	<p id="msg"></p>
	<!--Form start here-->
	<form action="../unregister/userinfo.php" method="post" id="register_form" enctype="multipart/form-data" >

	<label>user ID</label>
	<div class="input">
		<div>
			<input type="text" name="userID" id="name" readonly>
		</div>
		<div class="error u_error"></div>
	</div>

	<label>account ID</label>
	<div class="input">
		<div>
			<input type="text" name="acctID" id="name" >
		</div>
		<div class="error u_error"></div>
	</div>

	<label>User Image</label>
		<div class="input">
		<div>
			<input type="file"  class="btn btn-success" name="userImage">
		</div>
		<div class="error u_error"></div>
	</div>
<br>
	<label>Firstname</label>
	<div class="input">
		<div>
			<input type="text" name="userFirstname" value="">
		</div>
	</div>

	<label>Middlename</label>
	<div class="input">
		<div>
			<input type="text" name="userMiddlename" value="">
		</div>
	</div>

	<label>Lastname</label>
	<div class="input">
		<div>
			<input type="text" name="userLastname" value="">
		</div>
	</div>

	<label>Block No.</label>
	<div class="input">
		<div>
			<input type="text" name="userBlkno" value="">
		</div>
	</div>

	<label>Lot No.</label>
	<div class="input">
		<div>
			<input type="text" name="userLotno" value="">
		</div>
	</div>

	<label>Street</label>
	<div class="input">
		<div>
			<input type="text" name="userStreet" value="">
		</div>
	</div>

	<label>Barangay</label>
	<div class="input">
		<div>
			<input type="text" name="userBarangay" value="">
		</div>
	</div>

	<label>Municipality</label>
	<div class="input">
		<div>
			<input type="text" name="userMunicipality" value="">
		</div>
	</div>

	<label>Province</label>
	<div class="input">
		<div>
			<input type="text" name="userProvince" value="">
		</div>
	</div>

	<label>Contact No.</label>
	<div class="input">
		<div>
			<input type="text" name="userContactno" value="">
		</div>
	</div>

	<label>Birthday</label>
	<div class="input">
		<div>
			<input type="date" name="userBdate" min="1917-01-01" max="1997-01-01"  value="">
		</div>
	</div>

	<label>Gender</label>
	<div class="input">
		<div>
			<select class="input-opt" name="userGender">
				<option value="MALE">MALE</option>
				<option value="FEMALE">FEMALE</option>
			</select>
		</div>

		<div class="error"></div>
	</div>

	<label>User Status</label>
	<div class="input">
		<div>
			<select class="input-opt" name="userStatus">
				<option value="PENDING">PENDING</option>
				<!--<option value="VERIFIED">VERIFIED</option>
				<option value="BLOCK">BLOCK</option>-->
				
			</select>
		</div>

		<div class="error"></div>
	</div>




	
	<div class="input">
		<div>
			<input type="submit" class="btn btn-primary" name="submit" value="Submit">
		</div>
	</div>
	</form>
</div>

</body>
</html>