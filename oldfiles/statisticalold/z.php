<?php 
//include('header.php');
require_once('../database/db.php');
$theader=array(
				'#',
				'Veterinarian',
				'Permit No.',
				'Clinic',
				'Clinic No.',
				'Contact No.',
				'Block No.',
				'Lot No.',
				'Street',
				'Barangay',
				'Municipality',
				'Province'
			);
$tname=array(	
				'id',
				'userID',
				'vetPermitno',
				'vetClinic',
				'vetClinicno',
				'vetContactno',
				'vetBlkno',
				'vetLotno',
				'vetStreet',
				'vetBarangay',
				'vetMunicipality',
				'vetProvince'
			);
$cnt=count($tname);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

</div>

</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Large Modal</h2>
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Large Modal</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Veterinarian</h4>
        </div>
        <div class="modal-body">
			<h2></h2>
				<form method="post" action="add.php"  enctype="multipart/form-data" class="form-horizontal">
				
					<div class="form-group">
						<label class="control-label col-sm-2" for="userID">Veterinarian </label>
						<div class="col-sm-10">
							<select  name="userID" style="" class="form-control" >
								<?php 
								$resulta = $conn->prepare("SELECT acctID,acctName FROM account ORDER BY acctName ASC");
								$resulta->execute();
								for($ia=0; $rowa = $resulta->fetch(); $ia++){echo '<option value="'.$rowa['acctID'].'">'.$rowa['acctName'].'</option>';}	
								echo ' </select>';									
								?>
						</div>
					</div>

					<?php 
					$slbl='';
					$elbl='';
					for ($row=2;$row<$cnt;$row++) 
						{
							//echo $slbl.$theader[$row].$elbl;
					?>
					<div class="form-group">
						<label class="control-label col-sm-2" for="vetPermitno"><?=$theader[$row]?> </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="<?=$tname[$row]?>" placeholder="<?=$theader[$row]?>" name="<?=$tname[$row]?>">
							</div>
					</div>

					<?php
					}
					?>

					<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
					<button type="submit" name="Submit" class="btn btn-primary">Upload</button>
					</div>	
				</form>
        </div>
        
      </div>
    </div>
  </div>
</div>

</body>
</html>
