<?php include('header.php');
require_once('db.php');
?>
<body>

<div class="row-fluid">
	<div class="span12">
		<div class="container">
		<a class="btn btn-primary" href="#myModal" data-toggle="modal">Click Here To Add</a>
		<br><br>
		<a href="../administrator/navigationinline.php"><button class="btn btn-primary">Back</button></a>
<!-- Modal -->
	<div id="myModal" class="modal hide fade " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	<div class="modal-header"><h3 id="myModalLabel">Add</h3></div>
		<div class="modal-body">

		<form method="post" action="add.php"  enctype="multipart/form-data">
		<table class="table1">

		<?php 
			$theader=array(
				'#',
				'Veterinarian',
				'Permit No.',
				'Clinic',
				'Clinic No.',
				'Contact No.',
				'Block No.',
				'Lot No.',
				'Street',
				'Barangay',
				'Municipality',
				'Province'
			);
			$tname=array(	
				'id',
				'userID',
				'vetPermitno',
				'vetClinic',
				'vetClinicno',
				'vetContactno',
				'vetBlkno',
				'vetLotno',
				'vetStreet',
				'vetBarangay',
				'vetMunicipality',
				'vetProvince'
			);
			$cnt=count($theader);

			$slbl='<tr><td><label style="color:#3a87ad; font-size:18px;" >';
			$mlbl='</label></td><td width="30"></td><td><input type="text" name="';
			$elbl='" value=""></td>';
			for ($row=1;$row<$cnt;$row++) 
				{
					if ($row==1) 
					{
						echo '<td><label style="color:#3a87ad; font-size:18px;">Veterinarian</label></td><td width="30"></td>
						<td><select  name="userID" style="">';						
						$resulta = $con->prepare("SELECT acctID,acctName FROM account ORDER BY acctName ASC");
						$resulta->execute();
						for($ia=0; $rowa = $resulta->fetch(); $ia++){echo '<option value="'.$rowa['acctID'].'">'.$rowa['acctName'].'</option>';}	
						echo ' </select></td></tr>';						
					}
					else {echo $slbl.$theader[$row].$mlbl.$tname[$row].$elbl;}
				}

			echo '</tr>';
			
		?>
		</table>
		</div>


		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button type="submit" name="Submit" class="btn btn-primary">Upload</button>
		</div>
		</form>
</div>			


<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
	<div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<strong><i class="icon-user icon-large"></i>&nbsp;Veterinarian Information</strong>
	</div>
	<thead>

	<?php
	$slbl='<th style="text-align:center;">';
	$elbl='</th>';
	for ($row=0;$row<$cnt;$row++) {echo $slbl.$theader[$row].$elbl;}

	?>

	<th style="text-align:center;">Action</th>                                
	</thead>

	<tbody>
	<tr>

	<?php
	$result = $con->prepare("SELECT * FROM veterinariandetails ORDER BY id ASC");
	$result->execute();
	for($i=0; $row = $result->fetch(); $i++){
		$id=$row['id'];
		$slbl='<td style="text-align:center; word-break:break-all; width:200px;">';
		for ($xrow=0;$xrow<$cnt;$xrow++) 
		{
			if ($xrow==1) 
			{
				$resultsx = $conn->prepare("SELECT * FROM userinfo where userID=".$row['userID']);
				$resultsx->execute();
				for($iix=0; $rowsx = $resultsx->fetch(); $iix++){ echo $slbl.$rowsx['acctName'].'</td>'; }				
			}
		else 
			{echo $slbl.$row[$tname[$xrow]].'</td>';}
		}	
	?>

		<td style="text-align:center; width:350px;">
		<a href="#updte_img<?php echo $id;?>"  data-toggle="modal"  class="btn btn-warning" >Update</a>									 
		<a href="#delete<?php echo $id;?>"  data-toggle="modal"  class="btn btn-danger" >Delete </a>
		</td>
	</tr>
<!-- Modal -->
<div id="delete<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header"><h3 id="myModalLabel">Delete</h3></div>
	<div class="modal-body">
	<div class="alert alert-danger">

	<b style="color:blue; margin-left:25px; font-size:30px;"><?php echo $row['vetClinicno']." ".$row['vetClinic']; ?></b>
	<br />
	<p style="font-size: larger; text-align: center;">Are you Sure you want to Delete?</p>
</div>
	<hr>
	<div class="modal-footer">
		<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
		<a href="delete.php<?php echo '?id='.$id; ?>" class="btn btn-danger">Yes</a>
	</div>
</div>
</div>
<!-- Modal Update Image -->
<div id="updte_img<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header">
<h3 id="myModalLabel">Update</h3>

</div>

<div class="modal-body">
	<div class="alert alert-danger">

		<form action="edit.php<?php echo '?id='.$id; ?>" method="post" enctype="multipart/form-data">

			<?php
			$slbl='<label style="color:#3a87ad; font-size:18px;" >';
			$mlbl='<input type="text" name="';
			$elbl='" value="';

			for ($xrow=1;$xrow<$cnt;$xrow++) 
				{
				
					if ($xrow==1) 
						{											
						echo '<select  name="vuserID" style="">';						
						$resulta = $conn->prepare("SELECT * FROM account ORDER BY acctID ASC");
						$resulta->execute();
						for($ia=0; $rowa = $resulta->fetch(); $ia++){
						echo '<option value="'.$rowa['acctID'].'">'.$rowa['acctName'].'</option>';
						}	
						echo '		</select>
						</td>		
						</tr>';
						}

				echo $slbl.$theader[$xrow].'</label>'.$mlbl.$tname[$xrow].$elbl.$row[$tname[$xrow]].'">';
				}
			?>
			</div>
			<hr>
			<div class="modal-footer">
			<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
			<button type="submit" name="submit" class="btn btn-danger">Yes</button>
		</form>
	</div>
</div>
</div>
<?php } ?>
</tbody>
</table>



</div>
</div>
</div>
</div>


</body>
</html>


