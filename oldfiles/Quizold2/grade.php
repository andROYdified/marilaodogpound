<?php
include '../template/headeruser.php';
echo $_SESSION['acctID'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<title>PHP Quiz</title>
	
	<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body style="background-color: #FFECB3">

	<div id="page-wrap">

		<center><h1>Qualification Test Result</h1></center>
		
        
	<div class="input">
<center>
	
		<h3>Congratulations You have Passed the Qualification Test!</h3>
		<br>

		<div>
			<a href="../Quiz/matchingwelcome.php"><input type="submit" class="btn btn-primary" name="" value="PROCEED TO MATCHING TEST"></a>
		</div>
		<br>
		<br>
		<div>
			<a href="../register/home.php"><input type="submit" class="btn btn-primary" name="" value="BACK TO HOMEPAGE"></a>
		</div>
	</div>
</center>
	</div>
	
	<script type="text/javascript">
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript">
	var pageTracker = _gat._getTracker("UA-68528-29");
	pageTracker._initData();
	pageTracker._trackPageview();
	</script>

</body>

</html>