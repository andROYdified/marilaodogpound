<?php

include '../template/headeruser.php';

?>
<!DOCTYPE html>
<html>
<head>
	<title>Matching Test</title>
</head>
<style>
	body{
	font-family:arial;
	font-size:15px;
	line-height: 1.6em;
}
li{
	list-style: none;
}
a{
	text-decoration: none;
}
label{
	display: inline-block;
	width: 180px;
}
input[type='text']{
	width: 97%;
	padding: 4px;
	border-radius: 5px;
	border:1px #ccc solid;
}
input[type='number']{
	width: 50px;
	padding: 4px;
	border-radius: 5px;
	border:1px #ccc solid;
}
.container{
	width: 60%;
	margin:0 auto;
	overflow:auto;
}
header{
	border-bottom:3px #f4f4f4 solid;
}
footer{
	border-top: 3px #f4f4f4 solid;
	text-align: center;
	padding-top: 5px;
}
main{
	padding-bottom: 20px;
}
a.start{
	display: inline-block;
	color: #1B5E20;
	background-color: #00C853;
	border:1px dotted #000;
	padding:6px 13px;
}
.current{
	padding: 10px;
	background: #f4f4f4;
	border: #ccc dotted 1px;
	margin: 20px 0 10px 0;
}
@media only screen and (max-width: 960px){
	.container{
		width: 80%;
	}
}

</style>
<body style="background-color: #FFECB3">
<center><h2>Matching TEST</h2>
<br>
<br>
<h4>Please read and answer the following questions carefully and honestly.<br>
This test contains set of questions pertaining to your personal information<br>
and questions pertaining to ideal dog that you wanted to adopt</h4>
<br>
<br>
<a href="../Quiz/matchingtest.php"><input class="btn btn-primary" type="submit" value="Take a Test"></a>
</center>




			
</div>
</body>
</html>