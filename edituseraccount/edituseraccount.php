<?php include '../template/headeruser.php';

//$id = $_GET['id'];

include '../database/db.php';

$aID = $_SESSION['acctID']; 

$query = "SELECT * FROM account WHERE acctID=:acctID";
$stmt = $con->prepare($query);
$stmt -> bindParam(":acctID",$aID);

$stmt->execute();

$account = $stmt->fetch(PDO::FETCH_OBJ);



?>

<!DOCTYPE html>
<html>
<head>

<title>Edit Account</title>

</head>

<body style="background-color: #FFECB3">

<div class="form">
	<h2>Edit Account</h2>
	<hr>
	<p id="msg"></p>
	<!--Form start here-->
	<form action="update.php" method="post" id="register_form">
	<label>ID</label>
	<div class="input">
		<div>
			<input type="text" name="acctID" id="name" value="<?php echo $account->acctID ?>" readonly>
		</div>
		<div class="error u_error"></div>
	</div>
	<label>Email</label>
	<div class="input">
		<div>
			<input type="text" name="acctEmail" id="name" value="<?php echo $account->acctEmail ?>" autocomplete="off">
		</div>
		<div class="error u_error"></div>
	</div>
	<label>Password</label>
	<div class="input">
		<div>
			<input type="text" name="acctPassword" id="myInput" value="<?php echo $account->acctPassword; ?>" autocomplete="off">
		</div>
		<div class="error e_error"></div>
	</div>	

	<!--<label>User Type</label>
	<div class="input">
		<div>
			<select class="input-opt" name="userType">
				<option value="USER">USER</option>
				<option value="VETERINARIAN">VETERINARIAN</option>
				<option value="ADMINISTRATOR">ADMINISTRATOR</option>
			</select>
		</div>

		<div class="error"></div>
	</div>

	<label>UserID</label>
	<div class="input">
		<div>
			<input type="text" placeholder="userID" name="userID" id="name">
		</div>
		<div class="error u_error"></div>
	</div>-->
	
	<div class="input">
		<div>
			<input type="submit" class="btn btn-primary" name="UPDATE" value="UPDATE">
		</div>
	</div>


<!--	<tr>
				<td><input type="hidden" readonly name="id" value="<?php echo $student->id ?>"></td>
				<td><input type="submit" name="UPDATE RECORD"></td>
			</tr>-->
			<div class="password">
		<input type="checkbox" onclick="showHidepassword()" 
		>Show Password
	</div>
	</form>
	<br>
	<br>
	<!--<div>
		<a href="../register/userform.php">
			  &nbsp;&nbsp;CREATE USER INFORMATION
		</a>
	</div>
		<br>
	<br>
	<div>
		<a href="../userinformation/edituserinformation.php">
			  &nbsp;&nbsp;EDIT USER INFORMATION
		</a>
	</div>-->
</div>

</body>
<script type="text/javascript">
	function showHidepassword() {
		var x = document.getElementById("myInput");
		if (x.type === "password"){
			x.type = "text";
		}
		else{
			x.type = "password";
		}
					}				

</script>
</html>