<?php
/**
 * This file will show a simple implementation on how to send SMS using Chikka API
 * @author Ronald Allan Mojica
 * 
 */

    if(!isset($_POST)){
        header('Location: ./index.php');
        return;
    }

    $number = isset($_POST['number']) ? $_POST['number'] : "";
    $message = isset($_POST['message']) ? $_POST['message'] : "";
    $id = isset($_POST['id']) ? $_POST['id'] : 0;

    if($number == "" || $message == ""){
        $data["message"] = "All fields are required";

        echo json_encode($data);

        return;
    }


    include('ChikkaSMS.php');

    $clientId = 'ef047f01201806f1e69ad0c6558bfec7862633359320ccaa29698513a3360008';
    $secretKey = '28ccf8f525e045ef77478553e5b786a30536ae3fa34c15955c080277f563ebd5';
    $shortCode = '2929009338';
    $chikkaAPI = new ChikkaSMS($clientId,$secretKey,$shortCode);

    $uniqueID = time();
    $response = $chikkaAPI->sendText("$uniqueID", "$number", "$message");

    $data = array();

    if($response->message == 'ACCEPTED'){
        $data["message"] = "Message sent!";

        require_once('../database/db.php');

        $query = "UPDATE complainreport SET isNotified = 1 WHERE compID = $id";	
	    $stmt = $conn->exec($query);
    }
    else{
        $data["message"] = "Something went wrong. Please check the recipient's mobile number.";
    }

    echo json_encode($data);

?>