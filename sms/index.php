<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SMS Module</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
    <div>
        <div class="form-group">
            <label for="txtMobile">Mobile Number:</label>
            <input type="number" name="txtMobile" id="txtMobile" class="form-control" placeholder="eg: 639123456789">
        </div>
        <div class="form-group">
            <label for="txtMessage">Compose your message:</label>
            <textarea name="txtMessage" id="txtMessage" class="form-control" placeholder="Your message here" cols="30" rows="10"></textarea>
        </div>
        <div class="form-group">
            <button id="btnSend" class="btn btn-primary btn-lg">SEND</button>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){

            // perform button click event
            $("#btnSend").click(function(){
                var number = $("#txtMobile").val();
                var message = $("#txtMessage").val();

                $.ajax({
                    url : './sendSms.php',
                    data: { 'message' : message, 'number' : number},
                    method: 'POST',
                    dataType: 'json',
                    success: function(data){
                        alert(data.message);
                    }
                })
            });
        });
    </script>
</body>
</html>