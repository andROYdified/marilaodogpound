
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Dynamic Tabs</h2>
  <p>To make the tabs toggleable, add the data-toggle="tab" attribute to each link. Then add a .tab-pane class with a unique ID for every tab and wrap them inside a div element with class .tab-content.</p>

  <ul class="nav nav-tabs">
<?php
require_once('../database/db.php');

$sql = 'select * from questionmatching order by matchingNumber';
$result = $conn->prepare($sql);
$result->execute();				
$row = $result->fetchall();

$sql = 'select * from choicesmatching order by matchingChoicesID';
$result = $conn->prepare($sql);
$result->execute();				
$choices = $result->fetchall();

//print_r($row);		
?>
    <li class="active"><a data-toggle="tab" href="#menu1">Menu 1</a></li>    
    <li><a data-toggle="tab" href="#menu2">Menu 2</a></li>
    <li><a data-toggle="tab" href="#menu3">Menu 3</a></li>
  </ul>

  <div class="tab-content">
    <div id="menu1" class="tab-pane fade in active">     
		 <h3>Menu 1</h3><table class="table">
		  	<?php 
			$cnt=1;
			foreach($row as $key=>$value)
			{
				if ($value['4']==1) 
				{					
					echo '<p>'.$cnt.'. ' .$value['3'].'</p>';
					echo '<select  name="'.$value['0'].'" style="">';
					foreach($choices as $ckey=>$cvalue)
						{if ($cvalue['1']==$value['0']){echo '<option value="'.$value['0'].'">'.$cvalue['3'].'</option>';}}
					echo '		</select>';
					$cnt++;
				}
				
			}?> 
  
    </div>
    <div id="menu2" class="tab-pane fade">
      <h3>Menu 2</h3>
			<?php 
			$cnt=1;
			foreach($row as $key=>$value)
			{
				if ($value['4']==2) 
				{
					
					echo '<p>'.$cnt.'. ' .$value['3'].'</p>';
					echo '<select  name="'.$value['0'].'" style="">';
					foreach($choices as $ckey=>$cvalue)
					{
						if ($cvalue['1']==$value['0']) 	
						{
								echo '<option value="'.$value['0'].'">'.$cvalue['3'].'</option>';}
					}
					echo '		</select>';
					$cnt++;
				}
				
			}?> 
  
    </div>
    <div id="menu3" class="tab-pane fade">
      <h3>Menu 3</h3>
      		  <?php 
			$cnt=1;
			foreach($row as $key=>$value)
			{
				if ($value['4']==3) 
				{
					
					echo '<p>'.$cnt.'. ' .$value['3'].'</p>';
					echo '<select  name="'.$value['0'].'" style="">';
					foreach($choices as $ckey=>$cvalue)
					{
						if ($cvalue['1']==$value['0']) 	
						{
								echo '<option value="'.$value['0'].'">'.$cvalue['3'].'</option>';}
					}
					echo '		</select>';
					$cnt++;
				}
				
			}?>
    
    </div>
  </div>
</div>

</body>
</html>
