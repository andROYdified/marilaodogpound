
	    <!-- Button to trigger modal -->
    <a class="btn btn-primary" href="#myModal" data-toggle="modal">Click Here To Add</a>
	<br>
	<br>
	<br>
	<a href="../administrator/navigationinline.php"><button class="btn btn-primary">Back</button></a>
<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header">

<h3 id="myModalLabel">Add</h3>
</div>
<div class="modal-body">
<form method="post" action="upload.php"  enctype="multipart/form-data">
<table class="table1">


<tr>
		<td><label style="color:#3a87ad; font-size:18px;">Doginfo ID</label></td>
		<td width="30"></td>
		<td><input type="text" name="doginfoID" placeholder="id" readonly="" /></td>
	</tr>

	<tr>
		<td><label style="color:#3a87ad; font-size:18px;">Select your Image</label></td>
		<td width="30"></td>
		<td><input type="file" name="doginfoImage"></td>
	</tr>

	<tr>
		<td><label style="color:#3a87ad; font-size:18px;">Gender</label></td>
		<td width="30"></td>
		<td><select  name="doginfoGender" style="">
				<option value="MALE">MALE</option>
				<option value="FEMALE">FEMALE</option>
		</select>
		</td>
	</tr>
	<tr>
		<td><label style="color:#3a87ad; font-size:18px;">Breed</label></td>
		<td width="30"></td>
		<td><select  name="doginfoBreed" style="">
				<option value="PUREBREED">PUREBREED</option>
				<option value="MONGREL">MONGREL</option>
				<option value="MIXEDBREED">MIXEDBREED</option>
		</select>
		</td>
	</tr>
	<tr>
		<td><label style="color:#3a87ad; font-size:18px;">Color</label></td>
		<td width="30"></td>
		<td><select  name="doginfoColor" style="">
				<option value="WHITE">WHITE</option>
				<option value="BLACK">BLACK</option>
				<option value="BROWN">BROWN</option>
				<option value="MIXED">MIXED</option>
		</select>
		</td>
	</tr>
	<tr>
		<td><label style="color:#3a87ad; font-size:18px;">Size</label></td>
		<td width="30"></td>
		<td><select  name="doginfoSize" style="">
				<option value="SMALL">SMALL</option>
				<option value="MEDIUM">MEDIUM</option>
				<option value="LARGE">LARGE</option>
				
		</select>
		</td>
	</tr>
	<tr>
		<td><label style="color:#3a87ad; font-size:18px;">STATUS</label></td>
		<td width="30"></td>
		<td><select  name="doginfoStatus" style="">
				<option value="ADOPTABLE">ADOPTABLE</option>
				<option value="SURRENDERED">SURRENDERED</option>
				<option value="IMPOUNDED">IMPOUNDED</option>
				
				
		</select>
		</td>
	</tr>
	<tr>
		<td><label style="color:#3a87ad; font-size:18px;" readonly>Date Register</label></td>
		<td width="30"></td>
		<td><input type="hidden" name="doginfoDateRegister"
		value="">
		</td>
	</tr>
	<tr>
		<td><label style="color:#3a87ad; font-size:18px;" required>Score</label></td>
		<td width="30"></td>
		<td><input type="number" name="doginfoScore">
		</td>
	</tr>
	
	
</table>
    </div>
    <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
<button type="submit" name="Submit" class="btn btn-primary">Upload</button>
    </div>
</form>
</div>			