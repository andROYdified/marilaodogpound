<?php 

include('header.php');
echo '
<body><div class="row-fluid"><div class="span12">      
<div class="container">';

include ('modal_add.php'); 
?>
	<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">

		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong><i class="icon-user icon-large"></i>&nbsp;Question Matching</strong>
		</div>

		<thead>
			<!-- <tr>
			<th style="text-align:center;">DogID</th>-->
			<th style="text-align:center;">#</th>
			<th style="text-align:center;">Matching Question</th>
			<th style="text-align:center;">Matching Transtation</th>
			<th style="text-align:center;">Dog Questions</th>
			<th style="text-align:center;">Questions Part</th>

		</thead>
		
		<tbody>
		<?php
				require_once('../database/db.php');
			$result = $con->prepare("SELECT * FROM questionmatching");
			$result->execute();
			for($i=0; $row = $result->fetch(); $i++){
				$id=$row['matchingNumber'];		
				$shtm='<td style="text-align:center; word-break:break-all; width:200px;">';

				echo '<tr>';
				echo $shtm.$row ['matchingNumber'].'</td>';
				echo $shtm.$row ['matchingQuestion'].'</td>';
				echo $shtm.$row ['matchingTranslation'].'</td>';
				echo $shtm.$row ['dogquestion'].'</td>';
				echo $shtm.$row ['qpart'].'</td>';			
			?>			 
			
			<td style="text-align:center; width:350px;">
				<a href="#updte_img<?php echo $id;?>"  data-toggle="modal"  class="btn btn-warning" >Update</a><br>
				<a href="#delete<?php echo $id;?>"  data-toggle="modal"  class="btn btn-danger" >Delete </a>
			</td>
		</tr>

		<!-- Modal -->
		<div id="delete<?php  echo $id;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header"><h3 id="myModalLabel">Delete</h3></div>			
			<div class="modal-body">
				<div class="alert alert-danger">									
					<p style="font-size: larger; text-align: center;">Are you Sure you want to Delete?</p>
				</div>			
				<div class="modal-footer">
					<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">No</button>
					<a href="delete.php<?php echo '?id='.$id; ?>" class="btn btn-danger">Yes</a>
				</div>
			</div>
		</div>

		<?php include ('modal_edit.php'); 
}?>
</tbody>
</table>



</div>
</div>
</div>
</div>


</body>
</html>


